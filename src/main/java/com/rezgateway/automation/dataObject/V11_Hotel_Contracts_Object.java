package com.rezgateway.automation.dataObject;

import java.util.HashMap;


public class V11_Hotel_Contracts_Object {

	public String getConDateFrom() {
		return ConDateFrom;
	}

	public void setConDateFrom(String conDateFrom) {
		ConDateFrom = conDateFrom;
	}

	public String getConDateTo() {
		return ConDateTo;
	}

	public void setConDateTo(String conDateTo) {
		ConDateTo = conDateTo;
	}

	public String getContract_Name() {
		return Contract_Name;
	}

	public void setContract_Name(String contract_Name) {
		Contract_Name = contract_Name;
	}

	public String getCurrency() {
		return Currency;
	}

	public void setCurrency(String currency) {
		Currency = currency;
	}

	public String[] getDays_of_week() {
		return days_of_week;
	}

	public void setDays_of_week(String[] days_of_week) {
		this.days_of_week = days_of_week;
	}

	public String getInventoryAllotment_Types() {
		return InventoryAllotment_Types;
	}

	public void setInventoryAllotment_Types(String inventoryAllotment_Types) {
		InventoryAllotment_Types = inventoryAllotment_Types;
	}

	String Contract_Name = "first contract";

	String Currency = "USD";

	String ConDateFrom = "12-April-2018";

	String ConDateTo = "25-August-2018";

	String[] days_of_week = { "mo", "tu", "fr" };

	String InventoryAllotment_Types = "Guarantee";
	String[] Rooms = { "aaaaaaaaa", "Beach Villa", "King Junior Suite" };
	
	String[] RateOptions = { "Net Rate", "Adnt Adult Net Rate", "Fixed Sell Rate" ,"Min Nights","Max Nights","Blackout"};

	public String[] getRateOptions() {
		return RateOptions;
	}

	public void setRateOptions(String[] rateOptions) {
		RateOptions = rateOptions;
	}

	String SelectInventoryOptions[] = { "Cut off" };

	public String[] getSelectInventoryOptions() {
		return SelectInventoryOptions;
	}

	public void setSelectInventoryOptions(String[] selectInventoryOptions) {
		SelectInventoryOptions = selectInventoryOptions;
	}

	public String[] getRooms() {
		return Rooms;
	}

	public void setRooms(String[] rooms) {
		Rooms = rooms;
	}
	
	
	HashMap<String, String> roomrates = new HashMap<>();

	// Guarantee, Pre Purchased,General

}
