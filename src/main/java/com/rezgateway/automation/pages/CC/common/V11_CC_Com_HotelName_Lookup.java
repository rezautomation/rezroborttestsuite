package com.rezgateway.automation.pages.CC.common;

import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.rezgateway.automation.pages.backoffice.V11_CC_Hotel_Inventory_Rates_Contract_Selection_Page;
import com.rezgateway.automation.utill.DriverFactory;

public class V11_CC_Com_HotelName_Lookup extends V11_CC_Com_Lookup {

	public HashMap<String, String> getHoteLookupDetails() throws Exception {

		HashMap<String, String> map = new HashMap<String, String>();

		map.put("Hotelname", getElement("Label_LookUp_HoteName_Xpath").getText());
		map.put("HotelAdressLine1", getElement("Label_LookUp_HoteAdressline1_Xpath").getText());
		map.put("HotelAdressLine2", getElement("Label_LookUp_HoteAdressline2_Xpath").getText());
		map.put("City", getElement("Label_LookUp_City_Xpath").getText());
		map.put("State", getElement("Label_LookUp_State_Xpath").getText());
		map.put("Zip", getElement("Label_LookUp_Zip_Xpath").getText());
		map.put("Country", getElement("Label_LookUp_Country_Xpath").getText());
		map.put("SupplierName", getElement("Label_LookUp_SupplierName_Xpath").getText());
		
		System.out.println(map.get(1));

		return map;

	}
	
	public List<WebElement> isHotelAvailable() throws Exception{
		
		WebElement hotel = DriverFactory.getInstance().getDriver().findElement(By.xpath("/html/body/div[2]/div/div[1]/div/div/div[1]/div[2]/div[1]/ul"));
		List<WebElement> hotelList = hotel.findElements(By.tagName("li"));
		return hotelList;
	}
	
	public void selectFirstRecords() throws Exception {

		getElement("Label_first_Record_xpath").click();
	}
	
	
	public V11_CC_Hotel_Inventory_Rates_Contract_Selection_Page procedToInventoryRateContractSection() throws Exception{
		
		V11_CC_Hotel_Inventory_Rates_Contract_Selection_Page contracPage = new V11_CC_Hotel_Inventory_Rates_Contract_Selection_Page();
		getElement("button_cancel_linkText").click();
		
		return contracPage;
		
	}
	
}
