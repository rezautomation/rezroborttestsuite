package com.rezgateway.automation.pages.CC.common;

import java.util.HashMap;

import org.openqa.selenium.WebElement;

import com.rezgateway.automation.core.PageObject;
import com.rezgateway.automation.core.UIElement;
import com.rezgateway.automation.pages.backoffice.V11_CC_Hotel_ProfitMarkup_Page;

public class V11_CC_Com_HeaderPage extends PageObject {

	public String getHeader() throws Exception {

		String headername = new String();

		headername = getElement("Label_Title_id").getText();

		return headername;

	}

	public void logout() throws Exception {

		getElementClick("Link_Logout_xpath");
	}

	public HashMap<String, String> getproducts() throws Exception {

		HashMap<String, String> products = new HashMap<>();

		products.put("Flight", getElement("flight_Menu_id").getText());
		products.put("Hotel", getElement("Hotel_Menu_id").getText());
		products.put("Activity", getElement("Activity_Menu_id").getText());
		products.put("Transfer", getElement("Transfer_Menu_id").getText());
		products.put("Car", getElement("Car_Menu_id").getText());
		products.put("Package", getElement("Packages_Menu_id").getText());

		return products;

	}

	public boolean isHotelSetupConfigurationLinkAvailbile() throws Exception {

		//Header_Menu_id
		UIElement ele = getElement("Header_Menu_id");
		ele.getinnerWebElement(ele.getWebElement(), getElement("Hotel_Menu_id")).click();
		getElement("button_hotelSetup_xpath").click();
		//getElement("Hotel_Menu_id").click();
		String flag = getElement("button_hotelConfiguration_xpath").getText();
		if (flag.equalsIgnoreCase("Hotel Configuration")) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isHotelInventoryAndRateLinkAvailbile() throws Exception {
	
		UIElement ele = getElement("Header_Menu_id");
		ele.getinnerWebElement(ele.getWebElement(), getElement("Hotel_Menu_id")).click();
		getElement("button_hotelSetup_xpath").click();
		String flag = getElement("button_hotelInventoryAndRates_xpath").getText();
		if (flag.equalsIgnoreCase("Hotel Inventory Rate")) {
			return true;
		} else {
			return false;
		}
	}
	
	
	public WebElement isHotelProfitMarginSetupLinkAvailabile() throws Exception{
		
		UIElement ele = getElement("Header_Menu_id");
		ele.getinnerWebElement(ele.getWebElement(), getElement("Hotel_Menu_id")).click();
		ele = getElement("element_hotelMenuList_id");
		WebElement ele2 = ele.getinnerWebElement(ele.getWebElement(), getElement("button_hotelProfiMargin_id"));
	
		return ele2;
	}
	
	public V11_CC_Hotel_ProfitMarkup_Page proccedToProfitMarkUpPage() throws Exception{
		
		V11_CC_Hotel_ProfitMarkup_Page profitMarginPage = new V11_CC_Hotel_ProfitMarkup_Page();
		
		UIElement ele = getElement("element_hotelMenuList_id");
		ele.getinnerWebElement(ele.getWebElement(), getElement("button_hotelProfiMargin_id")).click();
		
		return profitMarginPage;
		
	}
	
	
	
	public void switchlanguage() throws Exception {

		String a = "English";

		if (a.equalsIgnoreCase("English")) {

			getElement("Dropdown_Logout_xpath").click();

		} else if (a.equalsIgnoreCase("Spanish")) {

			getElement("Dropdown_Logout_xpath").click();

		}

	}

}
