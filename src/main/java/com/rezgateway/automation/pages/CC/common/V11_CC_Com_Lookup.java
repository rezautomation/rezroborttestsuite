package com.rezgateway.automation.pages.CC.common;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.rezgateway.automation.utill.*;
import com.rezgateway.automation.core.PageObject;

@SuppressWarnings("unused")
public class V11_CC_Com_Lookup extends PageObject {

	public String GetlookupHeader() throws Exception {

		String HeaderText = "";

		HeaderText = getElement("Label_Lookup_Header_xpath").getText();

		return HeaderText;

	}
	
	public boolean isRecordAvailable() throws Exception {

		boolean status = false;

		status = getElement("Label_first_Record_xpath").isElementAvailable();

		return status;

	}


	public boolean isPaginationAvailable() throws Exception {

		boolean status = false;

		status = getElement("link_Pagination_xpath").isElementAvailable();

		return status;

	}

	public String getTotalNoOfRecords() throws Exception {

		String Recordcount = null;
		Recordcount = getElement("Label_Total_Records_xpath").getText();

		return Recordcount;

	}

	public void cancelLookUp() throws Exception {

		getElement("button_Cancel_lookup_xpath").click();

	}

	public boolean quickFiltersisAvailable() throws Exception {

		boolean status = false;

		status = getElement("Link_Quickfilter_xpath").isElementAvailable();

		return status;

	}

	public void selectFirstRecords() throws Exception {

		getElement("Label_first_Record_xpath").click();
	}

	public void loadLookUp() throws Exception {

		getElement("button_HotelName_lookup_class").click();

	}

}
