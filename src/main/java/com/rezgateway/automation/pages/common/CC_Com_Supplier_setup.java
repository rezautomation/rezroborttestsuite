package com.rezgateway.automation.pages.common;

import com.rezgateway.automation.core.PageObject;
import com.rezgateway.automation.Objects.SupplierSetupDetails;


public class CC_Com_Supplier_setup extends PageObject{

	public boolean checkAvailability()
	{
		return getElementVisibility("radiobutton_create_supplier_id", 10);
	}
	
	public void enterDetails(SupplierSetupDetails info) throws Exception
	{
		if(info.getAction().equals("create"))
		{
			getElement("radiobutton_create_supplier_id").click();
			getElement("inputfield_supplier_code_id").setText(info.getSupplierCode());
			getElement("inputfield_supplier_name_id").setText(info.getSupplierName());
			
			if(info.getProductType().equals("transfer"))
			{
				getElements("checkbox_prduct_type_id").get(2).click();
			}
			getElement("inputfield_address1_id").setText(info.getAddress1());
			getElement("inputfield_country_id").setText(info.getCountry());
			getElement("button_country_lookup_id").click();
			switchToFrame("frame_lookup_id");
			getElementIsClickable("button_country_1st_classname", 5);
			getElements("button_country_1st_classname").get(0).click();
			switchToDefaultFrame();
			getElement("inputfield_city_id").setText(info.getCity());
			getElement("button_city_lookup_id").click();
			switchToFrame("frame_lookup_id");
			getElementIsClickable("button_city_1st_classname", 5);
			getElements("button_city_1st_classname").get(0).click();
			switchToDefaultFrame();
			getElement("inputfield_currency_id").setText(info.getCurrency());
			getElement("button_currency_id").click();
			switchToFrame("frame_lookup_id");
			getElementIsClickable("button_currency_1st_classname", 5);
			getElements("button_currency_1st_classname").get(0).click();
			switchToDefaultFrame();
			getElement("button_save_id").click();
			
			try {
				getElementIsClickable("button_save_ok_xpath", 10);
				getElement("button_save_ok_xpath").click();
			} catch (Exception e) {
				// TODO: handle exception
				try {
					getElementVisibility("label_code_existing_id", 10);
					System.out.println(getElement("label_code_existing_id").getText());
					getElement("button_code_existing_xpath").click();		
				} catch (Exception e1) {
					// TODO: handle exception
				}
			}
			
			
			
		}
	}
}
