package com.rezgateway.automation.pages.common;


import com.rezgateway.automation.core.PageObject;

public class CC_Com_BO_Menu extends PageObject{

	public boolean isModuleImageAvailable()
	{
		try {
			switchToDefaultFrame();
			return getElementVisibility("image_module_image_id", 10);		
		} catch (Exception e) {
			switchToDefaultFrame();
			return false;
		}		
	}
	

}
