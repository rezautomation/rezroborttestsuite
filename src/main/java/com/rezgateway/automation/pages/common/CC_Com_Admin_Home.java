package com.rezgateway.automation.pages.common;

import java.util.HashMap;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.rezgateway.automation.utill.*;


@SuppressWarnings("unused")
public class CC_Com_Admin_Home extends CC_Com_HomePage{
	
	public CC_Com_Admin_Home() throws Exception {
		getElement("link_configurations_id").checkElementVisisbilityWithException(120, "CC_Com_Admin_Home");
	}
	
	
}
