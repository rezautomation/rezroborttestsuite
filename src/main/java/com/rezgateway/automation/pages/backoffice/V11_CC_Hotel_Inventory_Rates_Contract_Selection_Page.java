package com.rezgateway.automation.pages.backoffice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.rezgateway.automation.core.PageObject;
import com.rezgateway.automation.Objects.HotelContractInfo;

public class V11_CC_Hotel_Inventory_Rates_Contract_Selection_Page extends PageObject {

	public void selectSpecificContract(HotelContractInfo cont) throws Exception {

		ArrayList<WebElement> contrac_periods = new ArrayList<>(getElements("Label_Coontractperiod_Classname"));

		for (int i = 0; i < contrac_periods.size(); i++) {

			System.out.println(contrac_periods.get(i).getText());

			ArrayList<WebElement> contracts = new ArrayList<WebElement>(contrac_periods.get(i).findElements(By.xpath("following-sibling::div[@class='irate-scontract-names']")));

			for (WebElement ele : contracts) {

				System.out.println(ele.getText());

				System.out.println(cont.getPeriodFrom().replaceAll("/", "-"));

				if (contrac_periods.get(i).getText().equalsIgnoreCase("From " + cont.getPeriodFrom().replaceAll("/", "-") + " to " + cont.getPeriodTo().replaceAll("/", "-"))) {

					System.out.println(cont.getContractname() + " / " + cont.getCurrency());

					if (ele.getText().equalsIgnoreCase(cont.getContractname() + " / " + cont.getCurrency())) {

						ele.findElement(By.name("rate-contract-id")).click();

					}

				}

			}

		}

	}
	
	public ArrayList<WebElement> getInventoryContractList() throws Exception{
		
		ArrayList<WebElement> contrac_periods = new ArrayList<>(getElements("Label_Coontractperiod_Classname"));
		
		return contrac_periods;
	}
	
	public Map<WebElement, ArrayList<WebElement>> getRateContractMap() throws Exception{
		
		Map<WebElement, ArrayList<WebElement>>  getRateContractMap = new HashMap<WebElement, ArrayList<WebElement>>();
		
		for(WebElement contrac_period : getInventoryContractList() ){
			ArrayList<WebElement> contracts = new ArrayList<WebElement>(contrac_period.findElements(By.xpath("following-sibling::div[@class='irate-scontract-names']")));	
			getRateContractMap.put(contrac_period, contracts);
		}
		
		return getRateContractMap;
		
		
	}
	
}
