package com.rezgateway.automation.pages.backoffice;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.rezgateway.automation.Objects.HotelContractInfo;
import com.rezgateway.automation.core.PageObject;

public class V11_CC_Hotel_ProfitMarkupHotelSelection_Page extends PageObject {

	public void selectSupplier() {
		// ${TODO}: want to Implement new object structure for hotel and want to make better hotel profit mark-up screen testing.
		// ${TODO}: Implement to add any supplier
	}

	public List<WebElement> isSuppliersAvailable() {
		// ${TODO}: Implement to check suppliers are available
		return null;
	}

	public List<WebElement> isCountriesAvailable() {
		// ${TODO}: Implement to check countries are available
		List<WebElement> eleList = null;
		return eleList;
	}

	public WebElement selectCountry(String Country) throws Exception {

		switchToFrame("iframe_HotelProfitmarkup_id");
		waitForElementToLoad("dropdown_country_id", 3);
		getElement("dropdown_country_id").click();
		executejavascript("document.getElementsByName('conid')[0].value='" + Country + "';");
		// click the country drop-down
		/*

		// Select the country
		waitForElementToLoad("inputfeild_country_xpath", 3);
		getElement("inputfeild_country_xpath").setText(Country);
		waitForElementToLoad("inputfeild_country_xpath", 2);
		getElement("inputfeild_country_xpath").setText(Keys.ARROW_DOWN);
		getElement("inputfeild_country_xpath").setText(Keys.ENTER);*/

		// This is for test select correct Country
		WebElement uiEle = getElement("label_countryDropdown_xpath").getWebElement();
		switchToDefaultFrame();
		return uiEle;
	}

	public WebElement selectCity(String city) throws Exception {

		switchToFrame("iframe_HotelProfitmarkup_id");
		waitForElementToLoad("combobox_city_id", 3);
		//executejavascript("document.getElementsByName('start')[0].value='" + contractList.get(0).getPeriodFrom() + "';");
		executejavascript("document.getElementsByName('citid')[0].value='" + city + "';");
		
		// This is for test select correct city
		WebElement uiEle = getElement("label_cityDropdown_xpath").getWebElement();
		switchToDefaultFrame();
		return uiEle;
	}

	public void selectAllHotels() throws Exception {
		getElement("radio_Applicable_MarginAll_xpath").click();
	}

	public void selectMultipleHotel() {
		// ${TODO}: Implement the multiple hotel selection using new hotel object structure.
	}

	public void selecSingleHotel(HotelContractInfo contractInfo) throws Exception {

		switchToFrame("iframe_HotelProfitmarkup_id");
		waitForElementToLoad("radio_Applicable_MarginSingleHotel_xpath", 2);
		// Select the Single hotel radio button.
		getElement("radio_Applicable_MarginSingleHotel_xpath").click();
		
		waitForElementToLoad("combobox_singlehotelName_xpath", 2);
		// select the Hotel name
		getElement("combobox_singlehotelName_xpath").click();
		waitForElementToLoad("inputfeild_singlehotelname_xpath", 2);
		getElement("inputfeild_singlehotelname_xpath").setText(contractInfo.getHotelName());
		
		if (contractInfo.getHotelName().equals(getElement("label_avilableHotelName_xpath").getText())) {
			waitForElementToLoad("inputfeild_singlehotelname_xpath",5);
			getElement("inputfeild_singlehotelname_xpath").setText(Keys.ENTER);
			//getElement("dropdown_singlehotelName_xpath").setText(Keys.TAB);

			// select the rate contract
			// ${TODO}: currently we set profit margin to ALL Rate contract that will be change in to up-to rate plan level
			waitForElementToLoad("inputfeild_ratecontract_xpath", 3);
			getElement("inputfeild_ratecontract_xpath").click();
			getElement("inputfeild_ratecontract_xpath").setText("All");
			getElement("inputfeild_ratecontract_xpath").setText(Keys.ARROW_DOWN);
			getElement("inputfeild_ratecontract_xpath").setText(Keys.ENTER);

			// select the Inventory Allotment
			waitForElementToLoad("inputfeild_inventoryAllotment_xpath", 3);
			getElement("inputfeild_inventoryAllotment_xpath").click();
			getElement("inputfeild_inventoryAllotment_xpath").click();
			getElement("inputfeild_inventoryAllotment_xpath").setText("All");
			getElement("inputfeild_inventoryAllotment_xpath").setText(Keys.ARROW_DOWN);
			getElement("inputfeild_inventoryAllotment_xpath").setText(Keys.ENTER);

			// select the Room/Bed
			getElement("inputfeild_RoomBed_xpath").click();
			getElement("inputfeild_RoomBed_xpath").click();
			waitForElementToLoad("inputfeild_RoomBed_xpath", 1);
			getElement("inputfeild_RoomBed_xpath").setText("All");
			getElement("inputfeild_RoomBed_xpath").setText(Keys.ARROW_DOWN);
			getElement("inputfeild_RoomBed_xpath").setText(Keys.ENTER);
			switchToDefaultFrame();
		} else {
			switchToDefaultFrame();
			throw new Exception("Hotel Is not availbile in the Dropdown list");
		}
	}

	public V11_CC_Hotel_ProfitMarkupConfiguration_Page proccedToConfigurationPage() {

		V11_CC_Hotel_ProfitMarkupConfiguration_Page configuration = new V11_CC_Hotel_ProfitMarkupConfiguration_Page();
		return configuration;

	}

}
