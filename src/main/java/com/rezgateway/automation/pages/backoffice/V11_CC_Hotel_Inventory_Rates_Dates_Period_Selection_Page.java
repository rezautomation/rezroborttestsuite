package com.rezgateway.automation.pages.backoffice;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.rezgateway.automation.utill.*;
import com.rezgateway.automation.core.PageObject;
import com.rezgateway.automation.Objects.HotelContractInfo;

public class V11_CC_Hotel_Inventory_Rates_Dates_Period_Selection_Page extends PageObject {

	public void selectPeriodDatesSelectionoption(com.rezgateway.automation.dataObject.V11_Hotel_Contracts_Object con) throws Exception {

		if (con.getConDateFrom().equalsIgnoreCase("Period")) {

			getElement("RadioButton_period_id").click();

		}

		else if (con.getConDateFrom().equalsIgnoreCase("Specific Dates")) {

			getElement("RadioButton_dates_id").click();

		}

	}

	public void LoadCalenderFromDate() throws Exception {

		getElement("RadioButton_FromDateCal_xpath").click();

	}

	public void LoadCalenderToDate() throws Exception {

		getElement("RadioButton_ToDateCal_xpath").click();

	}

	@SuppressWarnings("deprecation")
	public void SelectDateperiod(String date) throws ParseException {

		int diffrence;

		// getElement("Link_CalenderRenameThis_xpath").click();

		DriverFactory.getInstance().getDriver().findElement(By.xpath("/html/body/div[3]/div/div[2]/div[1]/div[2]/div/div/span[1]")).click();

		ArrayList<WebElement> selectmonths = new ArrayList<>(DriverFactory.getInstance().getDriver().findElements(By.className("react-datepicker__month-option")));

		selectmonths.get(0).click();

		int currentyear = Calendar.getInstance().get(Calendar.YEAR);

		int FromYear = Integer.parseInt(date.split("/")[2].trim());

		if (FromYear == currentyear) {

			diffrence = (FromYear - currentyear);
		}

		else {
			diffrence = 12 + (FromYear - currentyear);

		}

		for (int i = 0; i < diffrence; i++) {
			// wait until UI Team adding changes
			DriverFactory.getInstance().getDriver().findElement(By.cssSelector("a.react-datepicker__navigation.react-datepicker__navigation--next")).click();
		}

		DriverFactory.getInstance().getDriver().findElement(By.xpath("/html/body/div[3]/div/div[2]/div[1]/div[2]/div/div/span[1]")).click();

		ArrayList<WebElement> months = new ArrayList<>(DriverFactory.getInstance().getDriver().findElements(By.className("react-datepicker__month-option")));

		for (WebElement ele : months) {

			System.out.println(ele.getText());

			String Dates = date.replaceAll("/", "-");

			Date d1 = new Date(Dates);

			SimpleDateFormat sdfNew = new SimpleDateFormat("dd-MMMM-YYYY");
			date = sdfNew.format(d1);

			if (date.split("-")[1].trim().equalsIgnoreCase(ele.getText())) {

				ele.click();
				break;

			}

		}

		ArrayList<WebElement> Days = new ArrayList<>(DriverFactory.getInstance().getDriver().findElements(By.className("react-datepicker__day")));

		for (WebElement element : Days) {
			try {
				System.out.println(element.getText());
				System.out.println(date.split("-")[0].trim().substring(1));

				if (date.split("/")[0].trim().startsWith("0")) {

					if (date.split("-")[0].trim().substring(1).equalsIgnoreCase(element.getText())) {

						String temp = "outside-month";
						if (!element.getAttribute("class").endsWith(temp)) {
							element.click();
							DriverFactory.getInstance().getDriver().findElement(By.xpath("//*[@id='root']/div/div[1]/div/div/div[1]/div[2]/div/div[2]/div[2]")).click();
							break;
						} else {
							System.out.println(element.getText() + "_outside-month_Date");
						}

					}

				} else {

					if (date.split("-")[0].trim().equalsIgnoreCase(element.getText())) {

						String temp = "outside-month";
						if (!element.getAttribute("class").endsWith(temp)) {
							element.click();
							DriverFactory.getInstance().getDriver().findElement(By.xpath("//*[@id='root']/div/div[1]/div/div/div[1]/div[2]/div/div[2]/div[2]")).click();
							break;
						} else {
							System.out.println(element.getText() + "_outside-month_Date");
						}

					}
				}
			} catch (Exception e) {
				System.out.println(e);

			}
		}

	}

	public void selectDaysOftheWeek(HotelContractInfo cont) {

		String days[] = cont.getDaysOfTheWeek().split("-");

		for (int i = 0; i < days.length; i++)

			if (days[i].equalsIgnoreCase("All")) {

				DriverFactory.getInstance().getDriver().findElement(By.xpath("//*[@id='root']/div/div[1]/div/div/div[1]/div[2]/div/div[2]/div[3]/div[2]/label/div/label[1]/input")).click();
			}

			else if (days[i].equalsIgnoreCase("su")) {

				DriverFactory.getInstance().getDriver().findElement(By.xpath("//*[@id='root']/div/div[1]/div/div/div[1]/div[2]/div/div[2]/div[3]/div[2]/label/div/label[2]/input")).click();
			}

			else if (days[i].equalsIgnoreCase("mo")) {

				DriverFactory.getInstance().getDriver().findElement(By.xpath("//*[@id='root']/div/div[1]/div/div/div[1]/div[2]/div/div[2]/div[3]/div[2]/label/div/label[3]/input")).click();
			}

			else if (days[i].equalsIgnoreCase("tu")) {

				DriverFactory.getInstance().getDriver().findElement(By.xpath("//*[@id='root']/div/div[1]/div/div/div[1]/div[2]/div/div[2]/div[3]/div[2]/label/div/label[4]/input")).click();
			}

			else if (days[i].equalsIgnoreCase("we")) {

				DriverFactory.getInstance().getDriver().findElement(By.xpath("//*[@id='root']/div/div[1]/div/div/div[1]/div[2]/div/div[2]/div[3]/div[2]/label/div/label[5]/input")).click();
			}

			else if (days[i].equalsIgnoreCase("th")) {

				DriverFactory.getInstance().getDriver().findElement(By.xpath("//*[@id='root']/div/div[1]/div/div/div[1]/div[2]/div/div[2]/div[3]/div[2]/label/div/label[6]/input")).click();
			}

			else if (days[i].equalsIgnoreCase("fr")) {

				DriverFactory.getInstance().getDriver().findElement(By.xpath("//*[@id='root']/div/div[1]/div/div/div[1]/div[2]/div/div[2]/div[3]/div[2]/label/div/label[7]/input")).click();
			}

			else if (days[i].equalsIgnoreCase("sa")) {

				DriverFactory.getInstance().getDriver().findElement(By.xpath("//*[@id='root']/div/div[1]/div/div/div[1]/div[2]/div/div[2]/div[3]/div[2]/label/div/label[8]/input")).click();
			}

	}

	public void clickAddButton() {

		DriverFactory.getInstance().getDriver().findElement(By.xpath("//*[@id='root']/div/div[1]/div/div/div[1]/div[2]/div/div[2]/div[3]/div[2]/label/div/a")).click();
	}

	public V11_CC_Select_Inventory_Allotment_Page  proceedInventoryAllotmentPage() {
		DriverFactory.getInstance().getDriver().findElement(By.xpath("//*[@id='root']/div/div[1]/div/div/div[1]/div[2]/div/div[2]/div[5]/a")).click();
		
		V11_CC_Select_Inventory_Allotment_Page inventoryTypePage = new V11_CC_Select_Inventory_Allotment_Page();
		
		return inventoryTypePage;
	
	}

}
