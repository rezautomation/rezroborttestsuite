package com.rezgateway.automation.pages.backoffice;

import org.openqa.selenium.WebElement;
import com.rezgateway.automation.core.PageObject;

public class V11_CC_Hotel_ProfitMarkup_Page extends PageObject {

	public WebElement isPageAvailabile() throws Exception {
		getElement("element_HotelProfitmarkup_xpath").checkElementVisisbilityWithException(1, "Hotel Profitmarkup Serivice is Down");
		WebElement ele = getElement("element_HotelProfitmarkup_xpath").getWebElement();
		return ele;
	}

	
	public V11_CC_Hotel_ProfitMarkupHotelSelection_Page proccedToHotelSelectionPage() {

		V11_CC_Hotel_ProfitMarkupHotelSelection_Page hotelSelectionPage = new V11_CC_Hotel_ProfitMarkupHotelSelection_Page();
		return hotelSelectionPage;
	}

	
	public V11_CC_Hotel_ProfitMarkupConfiguration_Page proccedToConfigurationPage() {
		V11_CC_Hotel_ProfitMarkupConfiguration_Page configurationPage = new V11_CC_Hotel_ProfitMarkupConfiguration_Page();
		return configurationPage;
	}

	
	// Proceed To Markup Select By Dates Page
	public V11_CC_Hotel_ProfitMarkupSelectByDates_Page proccedToMarkupSelectByDatesPage() throws Exception {

		getElement("radio_selectByDate_xpath").click();

		V11_CC_Hotel_ProfitMarkupSelectByDates_Page selecBydate = new V11_CC_Hotel_ProfitMarkupSelectByDates_Page();
		return selecBydate;
	}

	
	// Proceed To Markup Select By Period Page
	public V11_CC_Hotel_ProfitMarkupSelectByPeriod_Page proccedToMarkupSelectByPeriodPage() throws Exception {

		getElement("radio_selectByPeriod_xpath").click();

		V11_CC_Hotel_ProfitMarkupSelectByPeriod_Page selecByPeriod = new V11_CC_Hotel_ProfitMarkupSelectByPeriod_Page();
		return selecByPeriod;
	}

	// ${TODO}: ADD Other page for proceed "Margin Update"
}
