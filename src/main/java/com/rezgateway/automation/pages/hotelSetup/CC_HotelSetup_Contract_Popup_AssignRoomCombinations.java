package com.rezgateway.automation.pages.hotelSetup;

import java.util.ArrayList;

import org.openqa.selenium.Keys;

import com.rezgateway.automation.core.PageObject;
import com.rezgateway.automation.Objects.HotelRoomCombinations;
import com.rezgateway.automation.Objects.HotelSetupDetails;

public class CC_HotelSetup_Contract_Popup_AssignRoomCombinations extends PageObject {

	public boolean checkAvailability() {
		try {
			return getElementVisibility("inputfield_contract_name_xpath", 60);
		} catch (Exception e) {
			return false;
		}
	}

	@SuppressWarnings("unused")
	public boolean enterDetails(HotelSetupDetails setupDetails, String hotelName, ArrayList<String> contracts) throws Exception {
		ArrayList<HotelRoomCombinations> combinationList = new ArrayList<>();
		ArrayList<HotelRoomCombinations> contractList = new ArrayList<>();

		String contractname = "";
		for (int i = 0; i < setupDetails.getRoomCombinations().size(); i++) {
			if (setupDetails.getRoomCombinations().get(i).getHotelname().equals(hotelName)) {
				String combination = setupDetails.getRoomCombinations().get(i).getRoomType() + "/" + setupDetails.getRoomCombinations().get(i).getBedType() + "/" + setupDetails.getRoomCombinations().get(i).getRatePlan();
				if (!setupDetails.getRoomCombinations().get(i).getContractName().equals(contractname)) {
					contractname = setupDetails.getRoomCombinations().get(i).getContractName();
					getElement("inputfield_contract_name_xpath").clear();
					getElement("inputfield_contract_name_xpath").setText(contractname);
					waitElementLoad(5);

					getElement("inputfield_contract_name_xpath").setText(Keys.ARROW_DOWN);
					getElement("inputfield_contract_name_xpath").setText(Keys.ENTER);
					waitElementLoad(2);
				}

				getElementIsClickable("button_room_type_xpath", 10);
				waitElementLoad(5);
				getElement("button_room_type_xpath").click();

				System.out.println(combination);
				for (int j = 1; j > 0; j++) {
					try {
						System.out.println(getElement("label_room_type_xpath").changeRefAndGetElement(getElement("label_room_type_xpath").getRef().replace("replace", String.valueOf(j))).getText());
						if (combination.equalsIgnoreCase(getElement("label_room_type_xpath").changeRefAndGetElement(getElement("label_room_type_xpath").getRef().replace("replace", String.valueOf(j))).getText())) {
							getElement("checkbox_room_type_xpath").changeRefAndGetElement(getElement("checkbox_room_type_xpath").getRef().replace("replace", String.valueOf(j))).click();
							break;
						}
					} catch (Exception e) {
						break;
					}
				}
				getElementIsClickable("button_add_classname", 10);
				getElement("button_add_classname").click();
			}
		}
		if (getElementIsClickable("button_save_classname", 10)) {
			//getElement("button_save_classname").click();
			getElement("button_save_xpath").click();

			if (getelementPresence(getElement("label_SaveAlert_xpath"))) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}

	}
	
	public CC_HotelSetup_Contract_Popup_TaxDetails proceedToTaxDetailsScreen() throws Exception{
		
		CC_HotelSetup_Contract_Popup_TaxDetails taxDetails =  new CC_HotelSetup_Contract_Popup_TaxDetails();
		getElement("button_LinkTohotelTaxDetails_xpath").click();
		
		if(getelementAvailability("button_doYouWantSaveNO_xpath"))
			getElement("button_doYouWantSaveNO_xpath").click();
		if(taxDetails.checkAvailability()){
			return taxDetails;	
		}else{
			return null;
		}
		
	}
	
	public CC_HotelSetup_Contract_Popup_BillingInfo proccedToBillingInfo() throws Exception {

		CC_HotelSetup_Contract_Popup_BillingInfo billingInfo = new CC_HotelSetup_Contract_Popup_BillingInfo();
		
		getElement("button_LinkToBillingInfo_xpath").click();
		
		if(getelementAvailability("button_doYouWantSaveNO_xpath"))
			getElement("button_doYouWantSaveNO_xpath").click();
		
		waitElementLoad(3);
		if (billingInfo.checkAvaialbilty()) {
			return billingInfo;
		} else {
			return null;
		}

	}
	
	
}
