package com.rezgateway.automation.pages.hotelSetup;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.rezgateway.automation.Objects.HotelCancellationPolicyDetails;
import com.rezgateway.automation.Objects.HotelContractInfo;
import com.rezgateway.automation.core.PageObject;
import com.rezgateway.automation.utill.DriverFactory;


public class CC_HotelSetup_Contract_CancellationPolicy_Page extends PageObject {

	static Logger logger;

	// ${TODO}: want to add cnx policy for same period but different inventory Contract.
	// ${TODO}: want to upgrade this code into adding cnx policy up to rate plan level and room wise.
	// ${TODO}: want to add multiple cnx policy within the same rate contract.
	
	
	public CC_HotelSetup_Contract_CancellationPolicy_Page() {
		logger = Logger.getLogger(CC_HotelSetup_Contract_CancellationPolicy_Page.class);
	}
	
	public List<WebElement> isPageAvilabile(){
		
		DriverFactory.getInstance().getDriver().switchTo().frame("hotelStandardInfoIframe");
		WebElement ele = DriverFactory.getInstance().getDriver().findElement(By.id("contractInfoAccordian"));
		List<WebElement> eleList = ele.findElements(By.className("panel-heading"));
		logger.debug("Element count is >>>> "+eleList.size());
		return eleList;
		
	}
	

	// this is for Contract Level Cancellation policy
	public boolean enterDetails(ArrayList<HotelCancellationPolicyDetails> cancellationPolicyInfo ,ArrayList<HotelContractInfo> inventoryContract ) throws Exception {

		DriverFactory.getInstance().getDriver().switchTo().frame("hotelStandardInfoIframe");
		int i = 0;
		boolean flag = false;

		for (HotelCancellationPolicyDetails tempObj : cancellationPolicyInfo) {

			String inContractID = tempObj.getHotelContract();
			String ContractlinkText = selectInventoryContract(inventoryContract, inContractID);
			// Click the Inventory Contract
			DriverFactory.getInstance().getDriver().findElement(By.linkText(ContractlinkText));
			// Click the rateContract
			DriverFactory.getInstance().getDriver().findElement(By.partialLinkText(tempObj.getRateContract())).click();
			// Open the CnX policy Screen
			getElement("button_addCNXPolicy_Common_xpath").changeRefAndGetElement(getElement("button_addCNXPolicy_Common_xpath").getRef().replace("j", Integer.toString(i))).click();

			executejavascript("document.getElementsByName('travelFrom')[0].value='" + tempObj.getFrom() + "';");
			executejavascript("document.getElementsByName('travelTo')[0].value='" + tempObj.getTo() + "';");

			getElement("textbox_cancellationBuffer_xpath").clear();
			getElement("textbox_cancellationBuffer_xpath").setText(tempObj.getCancellationBuffer());
			getElement("textbox_arrivaLessThanDays_xpath").setText(tempObj.getArrivalLessThan());

			switch (tempObj.getStdBasedOn()) {
			case "value":
				getElement("radio_StdBasedOnValueAmount_xpath").click();
				break;
			case "percentage":
				getElement("radio_StdBasedOnValuePercent_xpath").click();
				break;
			case "nights":
				getElement("radio_StdBasedOnValueNonights_xpath").click();
				break;
			}
			getElement("textbox_StdBasedOnValue_xpath").clear();
			getElement("textbox_StdBasedOnValue_xpath").click();

			switch (tempObj.getNoShowFeeBasedOn()) {
			case "value":
				getElement("radio_NoShowBasedOnValueAmount_xpath").click();
				break;
			case "percentage":
				getElement("radio_NoShowStdBasedOnValuePercent_xpath").click();
				break;
			case "nights":
				getElement("radio_NoShowStdBasedOnValueNonights_xpath").click();
				break;
			}

			getElement("textbox_NoShowBasedOnValue_xpath").setText(tempObj.getNoShowValue());
			getElement("button_AddPolicy_xpath").click();
			
			//check the entered Data is available in the Grid
			if(isDetailsAddingToTheGrid()){
				//give the saved status
				flag = isSaveTheDetails(); 
				logger.debug(inContractID+"_"+tempObj.getRateContract()+"_saving status >>>>>> "+ flag);
				getElement("button_closeCNXpolicyScreen_xpath").click();
			}else{
				logger.error(inContractID+"_"+tempObj.getRateContract()+"_cnx Data adding status >>>>>> " +flag);
				return flag;
			}
		}
		return flag;
	}

	//check the data is available in the table
	public boolean isDetailsAddingToTheGrid() throws Exception {
		boolean flag = false;
		WebElement table = getElement("tabel_DataGrid_xpath").getWebElement();
		List<WebElement> rows = table.findElements(By.className("rowData"));

		for (WebElement row : rows) {
			List<WebElement> cols = row.findElements(By.tagName("td"));
			for (WebElement cel : cols) {
				String temp = cel.getText();
				if ((null != temp) && (!temp.isEmpty())) {
					flag = true;
				} else {
					logger.debug("some data is missing in the grid_ " );
					flag = false;
				}
			}
		}
		return flag;
	}

	//check the data saving
	public boolean isSaveTheDetails() throws Exception {
		boolean flag = false;

		if (getElement("button_save_xpath").isElementAvailable()) {
			getElement("button_save_xpath").click();
			waitElementLoad(2);
			if (getElement("label_saveDetials_xpath").getText().equals("Data Saved Successfully")) {
				flag = true;
			} else {
				flag = false;
			}
		} else {
			flag = false;
		}

		return flag;
	}

	// Choosing the Inventory Contract
	public String selectInventoryContract(ArrayList<HotelContractInfo> inventoryContract, String name) {

		String InvContracName = null;
		// 1 Jun 2018 - 9 Jun 2018
		for (HotelContractInfo tempObj : inventoryContract) {

			if (name.equals(tempObj.getHotelContractNumber())) {
				String from = tempObj.getPeriodFrom().replaceAll("/", " ");
				logger.debug(from);
				String To = tempObj.getPeriodTo().replaceAll("/", " ");
				logger.debug(To);
				InvContracName = from.replace("0", "") + " - " + To.replace("0", "");
			}
		}
		return InvContracName;

	}

}
