package com.rezgateway.automation.pages.hotelSetup;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.poi.ss.formula.functions.Value;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.rezgateway.automation.core.PageObject;
import com.rezgateway.automation.utill.DriverFactory;
import com.rezgateway.automation.Objects.HotelContractInfo;
import com.rezgateway.automation.Objects.HotelRoomAndInventoryInfo;
import com.rezgateway.automation.Objects.HotelSetupDetails;
import com.rezgateway.automation.Objects.HotelStandardInfo;


//${TODO}: want refactor the object structure of the All hotel setup pojos

@SuppressWarnings("unused")
public class CC_HotelSetup_Contract extends PageObject {

	HotelSetupDetails setupDetails;
	String hotelname;
	static Logger logger;

	public CC_HotelSetup_Contract(HotelSetupDetails setupDetails, String hotelname) {
		this.hotelname = hotelname;
		this.setupDetails = setupDetails;

	}

	public CC_HotelSetup_Contract() {
		logger = Logger.getLogger(CC_HotelSetup_Standard.class);
	}

	public boolean checkAvailability() {
		if (getElementVisibility("inputfield_hotel_name_xpath", 60))
			return true;
		else
			return false;
	}

	public boolean createRoomCombination() throws Exception {

		getElement("inputfield_hotel_name_xpath").clear();
		getElement("inputfield_hotel_name_xpath").setText(hotelname);
		waitElementLoad(3);
		getElementVisibility("button_hotel_lookup_xpath", 1);
		getElement("button_hotel_lookup_xpath").click();
		waitElementLoad(3);
		getElementVisibility("button_hotel_1st_record_xpath", 1);
		getElementIsClickable("button_hotel_1st_record_xpath", 1);
		getElement("button_hotel_1st_record_xpath").click();

		ArrayList<HotelRoomAndInventoryInfo> roomList = new ArrayList<>();
		for (int i = 0; i < setupDetails.getInventoryInfo().size(); i++) {
			if (setupDetails.getInventoryInfo().get(i).getHotelName().equals(hotelname)) {
				roomList.add(setupDetails.getInventoryInfo().get(i));
			}
		}

		int roomCount = getElements("checkbox_roomType_classname").size();
		int bedCount = getElements("checkbox_bedType_classname").size();
		int ratePlanCount = getElements("checkbox_ratePlan_classname").size();
		int iterator = 0;
		if (roomCount > bedCount)
			iterator = roomCount;
		else
			iterator = bedCount;

		for (int j = 0; j < roomList.size(); j++) {
			waitElementLoad(8);
			for (int i = 1; i < (roomCount + 1); i++) {

				if (getElement("label_room_type_xpath").changeRefAndGetElement(getElement("label_room_type_xpath").getRef().replace("replace", String.valueOf(i + 1))).getText().trim().equalsIgnoreCase(roomList.get(j).getRoomType())) {
					getElement("checkbox_room_type_xpath").changeRefAndGetElement(getElement("checkbox_room_type_xpath").getRef().replace("replace", String.valueOf(i + 1))).click();
					break;
				}
			}

			for (int i = 1; i < (bedCount + 1); i++) {
				if (getElement("label_bed_type_xpath").changeRefAndGetElement(getElement("label_bed_type_xpath").getRef().replace("replace", String.valueOf(i + 1))).getText().trim().contains(roomList.get(j).getBedType())) {
					getElement("checkbox_bed_type_xpath").changeRefAndGetElement(getElement("checkbox_bed_type_xpath").getRef().replace("replace", String.valueOf(i + 1))).click();
					break;
				}
			}

			for (int i = 2; i < (ratePlanCount + 3); i++) {
				if (getElement("label_rate_plan_xpath").changeRefAndGetElement(getElement("label_rate_plan_xpath").getRef().replace("replace", String.valueOf(i))).getText().trim().equalsIgnoreCase(roomList.get(j).getRatePlan())) {
					getElement("checkbox_rateplan_xpath").changeRefAndGetElement(getElement("checkbox_rateplan_xpath").getRef().replace("replace", String.valueOf(i))).click();
					break;
				}
			}
			waitElementLoad(3);
			getElementVisibility("button_create_combinations_xpath", 5);
			getElementIsClickable("button_create_combinations_xpath", 10);
			getElement("button_create_combinations_xpath").click();
			waitElementLoad(3);
			getElementVisibility("inputfield_std_adults_name", 5);
			executejavascript("document.getElementsByName('std-adults')[" + j + "].value='" + roomList.get(j).getStdAdults() + "';");
			executejavascript("document.getElementsByName('adnt-adults')[" + j + "].value='" + roomList.get(j).getAdntAdults() + "';");
			String roomID = getElements("inputfield_std_adults_name").get(j).getAttribute("id").split("-")[2];
			if (roomList.get(j).getChildrenAllowed().equals("yes")) {
				if (getElementVisibilityForNewRef(getElement("radiobutton_chidren_allowed_yes_id").getRef().replace("replace", roomID), 5)) {
					getElement("radiobutton_chidren_allowed_yes_id").changeRefAndGetElement(getElement("radiobutton_chidren_allowed_yes_id").getRef().replace("replace", roomID)).click();
				} else {
					waitElementLoad(2);
					getElement("radiobutton_chidren_allowed_yes_id").changeRefAndGetElement(getElement("radiobutton_chidren_allowed_yes_id").getRef().replace("replace", roomID)).click();

				}

				if (roomList.get(j).getMultiChildAge().equals("yes")) {
					getElement("checkbox_multi_child_age_name").changeRefAndGetElement(getElement("checkbox_multi_child_age_name").getRef().replace("replace", roomID)).click();
					getElement("checkbox_multi_child_age_name").changeRefAndGetElement(getElement("checkbox_multi_child_age_name").getRef().replace("replace", roomID)).click();
				}
				executejavascript("document.getElementsByName('max-children')[" + j + "].value='" + roomList.get(j).getMaxChildren() + "';");

			}

			waitElementLoad(3);
			getElements("inputfield_max_occupancy_name").get(j).clear();
			int temp = 0;
			if (roomList.get(j).getChildrenAllowed().equals("yes")) {
				temp = Integer.parseInt(roomList.get(j).getStdAdults()) + Integer.parseInt(roomList.get(j).getMaxChildren()) + Integer.parseInt(roomList.get(j).getAdntAdults());
				executejavascript("document.getElementsByName('max-occupancy')[" + j + "].value='" + temp + "';");
			} else {
				temp = Integer.parseInt(roomList.get(j).getStdAdults()) + Integer.parseInt(roomList.get(j).getAdntAdults());
				executejavascript("document.getElementsByName('max-occupancy')[" + j + "].value='" + temp + "';");
			}

			waitElementLoad(3);
			getElementIsClickable("button_room_combinations_save_xpath", 10);
			getElement("button_room_combinations_save_xpath").click();
			waitElementLoad(3);

		}

		Alert alert = DriverFactory.getInstance().getDriver().switchTo().alert();
		String getAlertText = alert.getText();

		if (getAlertText.equalsIgnoreCase("Data saved successfully")) {
			return true;
		} else {
			return false;
		}

	}

	public boolean addInventryInformationDeatils() throws Exception {

		boolean flag = false;
		switchToDefaultFrame();
		switchToFrame("frame_standard_id");

		ArrayList<HotelRoomAndInventoryInfo> roomList = new ArrayList<>();
		for (int i = 0; i < setupDetails.getInventoryInfo().size(); i++) {
			if (setupDetails.getInventoryInfo().get(i).getHotelName().equals(hotelname)) {
				roomList.add(setupDetails.getInventoryInfo().get(i));
			}
		}
		// select inventory type
		waitElementLoad(3);
		getElementIsClickable("checkbox_inventory_by_name", 10);
		if (roomList.get(0).getInventoryTypeFreeSell().equals("yes"))
			getElements("checkbox_inventory_by_name").get(1).click();

		waitElementLoad(1);
		if (roomList.get(0).getInventoryTypeOnReq().equals("yes"))
			getElements("checkbox_inventory_by_name").get(2).click();

		waitElementLoad(3);
		if (roomList.get(0).getInventoryTypeAllotment().equals("no"))
			getElements("checkbox_inventory_by_name").get(0).click();

		getElementIsClickable("button_inventory_info_save_xpath", 10);
		getElement("button_inventory_info_save_xpath").click();

		Alert alert = DriverFactory.getInstance().getDriver().switchTo().alert();
		String getAlertText = alert.getText();

		if (getAlertText.equalsIgnoreCase("Data saved successfully")) {
			flag = true;
		} else {
			flag = false;
		}
		return flag;
	}

	public int getContractCount() {
		int count = 1;
		String temp;
		ArrayList<HotelContractInfo> invContract = setupDetails.getContractinfo();

		for (HotelContractInfo tempObj : invContract) {

			if (Integer.toString(count).equals(tempObj.getHotelContractNumber())) {
				logger.debug("count and Hotel Contract name is same");
			} else {
				count++;
			}
		}

		return count;
	}

	public boolean isContractSave(){
		
		boolean flag = false;
		DriverFactory.getInstance().getDriver().switchTo().frame("hotelStandardInfoIframe");
		WebElement ele = DriverFactory.getInstance().getDriver().findElement(By.id("contractInfoAccordian"));
		
		List<WebElement> eleList = ele.findElements(By.className("panel-heading"));
		if(getContractCount()== eleList.size()){
			flag = true;
		}else{
			flag = false;
		}
		
		
		return flag;
	}
	
	
	public CC_HotelSetup_Contract_CancellationPolicy_Page proccedToCancellationPolicyPage(){
		
		CC_HotelSetup_Contract_CancellationPolicy_Page cnxPage = new CC_HotelSetup_Contract_CancellationPolicy_Page();
		
		return cnxPage;
		
		
	}
	
	
	public CC_HotelSetup_Contract_Popup_ContractDetails proccedToConTractDetailsPopup() throws Exception {

		CC_HotelSetup_Contract_Popup_ContractDetails contractPopUpScreen = new CC_HotelSetup_Contract_Popup_ContractDetails();

		getElement("button_add_hotel_contract_xpath").click();
		if (contractPopUpScreen.checkAvailability()) {
			return contractPopUpScreen;
		} else {
			return null;
		}

	}

}
