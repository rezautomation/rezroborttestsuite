package com.rezgateway.automation.Objects;

import java.util.ArrayList;


public class HotelStandardInfo {

	String hotelName, hotelGroup, starCategory, numberOfRooms, pmsHotel, childrenAllowed, multipleChildAgeGroup, numberOfGroups, 
	ages, supplierName, isHotelActive, featuredHotel, transferincluded, rateContract, ratesSetupBy, ratesBy, 
	applyCancellationPolicyBy, partnerType, shoppingCart, dynamicPackage, fixedPackage, address1, address2, country, 
	state, city, suburb, zipcode, contactname, email, contactType, contactMedia, execute; 
	
	ArrayList<HotelRoomAndInventoryInfo> inventoryDetails = new ArrayList<>();
	
	public HotelStandardInfo getDetails(String[] values)
	{
		
		this.setHotelName					(values[0]);					
		this.setHotelGroup					(values[1]); 				
		this.setStarCategory				(values[2]);
		this.setNumberOfRooms				(values[3]);
		this.setPmsHotel					(values[4]);
		this.setChildrenAllowed				(values[5]);
		this.setMultipleChildAgeGroup		(values[6]);
		this.setNumberOfGroups				(values[7]);
		this.setAges						(values[8]);
		this.setSupplierName				(values[9]);
		this.setIsHotelActive				(values[10]);
		this.setFeaturedHotel				(values[11]);
		this.setTransferincluded			(values[12]);
		this.setRateContract				(values[13]);
		this.setRatesSetupBy				(values[14]);
		this.setRatesBy						(values[15]);
		this.setApplyCancellationPolicyBy	(values[16]);
		this.setPartnerType					(values[17]);
		this.setShoppingCart				(values[18]);
		this.setDynamicPackage				(values[19]);
		this.setFixedPackage				(values[20]);
		this.setAddress1					(values[21]);
		this.setAddress2					(values[22]);
		this.setCountry						(values[23]);
		this.setState						(values[24]);
		this.setCity						(values[25]);
		this.setSuburb						(values[26]);
		this.setZipcode						(values[27]);
		this.setContactname					(values[28]);
		this.setEmail						(values[29]);
		this.setContactname					(values[30]);
		this.setContactType					(values[31]);
		this.setExecute						(values[32]);
		
		return this;
		
	}

	
	
	public ArrayList<HotelRoomAndInventoryInfo> getInventoryDetails() {
		return inventoryDetails;
	}



	public void setInventoryDetails(
			ArrayList<HotelRoomAndInventoryInfo> inventoryDetails) {
		this.inventoryDetails = inventoryDetails;
	}



	public String getContactType() {
		return contactType;
	}



	public void setContactType(String contactType) {
		this.contactType = contactType;
	}



	public String getContactMedia() {
		return contactMedia;
	}



	public void setContactMedia(String contactMedia) {
		this.contactMedia = contactMedia;
	}



	public String getExecute() {
		return execute;
	}

	public void setExecute(String execute) {
		this.execute = execute;
	}



	public String getHotelName() {
		return hotelName;
	}

	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}

	public String getHotelGroup() {
		return hotelGroup;
	}

	public void setHotelGroup(String hotelGroup) {
		this.hotelGroup = hotelGroup;
	}

	public String getStarCategory() {
		return starCategory;
	}

	public void setStarCategory(String starCategory) {
		this.starCategory = starCategory;
	}

	public String getNumberOfRooms() {
		return numberOfRooms;
	}

	public void setNumberOfRooms(String numberOfRooms) {
		this.numberOfRooms = numberOfRooms;
	}

	public String getPmsHotel() {
		return pmsHotel;
	}

	public void setPmsHotel(String pmsHotel) {
		this.pmsHotel = pmsHotel;
	}

	public String getChildrenAllowed() {
		return childrenAllowed;
	}

	public void setChildrenAllowed(String childrenAllowed) {
		this.childrenAllowed = childrenAllowed;
	}

	public String getMultipleChildAgeGroup() {
		return multipleChildAgeGroup;
	}

	public void setMultipleChildAgeGroup(String multipleChildAgeGroup) {
		this.multipleChildAgeGroup = multipleChildAgeGroup;
	}

	public String getNumberOfGroups() {
		return numberOfGroups;
	}

	public void setNumberOfGroups(String numberOfGroups) {
		this.numberOfGroups = numberOfGroups;
	}

	public String getAges() {
		return ages;
	}

	public void setAges(String ages) {
		this.ages = ages;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getIsHotelActive() {
		return isHotelActive;
	}

	public void setIsHotelActive(String isHotelActive) {
		this.isHotelActive = isHotelActive;
	}

	public String getFeaturedHotel() {
		return featuredHotel;
	}

	public void setFeaturedHotel(String featuredHotel) {
		this.featuredHotel = featuredHotel;
	}

	public String getTransferincluded() {
		return transferincluded;
	}

	public void setTransferincluded(String transferincluded) {
		this.transferincluded = transferincluded;
	}

	public String getRateContract() {
		return rateContract;
	}

	public void setRateContract(String rateContract) {
		this.rateContract = rateContract;
	}

	public String getRatesSetupBy() {
		return ratesSetupBy;
	}

	public void setRatesSetupBy(String ratesSetupBy) {
		this.ratesSetupBy = ratesSetupBy;
	}

	public String getRatesBy() {
		return ratesBy;
	}

	public void setRatesBy(String ratesBy) {
		this.ratesBy = ratesBy;
	}

	public String getApplyCancellationPolicyBy() {
		return applyCancellationPolicyBy;
	}

	public void setApplyCancellationPolicyBy(String applyCancellationPolicyBy) {
		this.applyCancellationPolicyBy = applyCancellationPolicyBy;
	}

	public String getPartnerType() {
		return partnerType;
	}

	public void setPartnerType(String partnerType) {
		this.partnerType = partnerType;
	}

	public String getShoppingCart() {
		return shoppingCart;
	}

	public void setShoppingCart(String shoppingCart) {
		this.shoppingCart = shoppingCart;
	}

	public String getDynamicPackage() {
		return dynamicPackage;
	}

	public void setDynamicPackage(String dynamicPackage) {
		this.dynamicPackage = dynamicPackage;
	}

	public String getFixedPackage() {
		return fixedPackage;
	}

	public void setFixedPackage(String fixedPackage) {
		this.fixedPackage = fixedPackage;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getSuburb() {
		return suburb;
	}

	public void setSuburb(String suburb) {
		this.suburb = suburb;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getContactname() {
		return contactname;
	}

	public void setContactname(String contactname) {
		this.contactname = contactname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	
}
