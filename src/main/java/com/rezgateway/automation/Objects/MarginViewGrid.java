package com.rezgateway.automation.Objects;

public class MarginViewGrid {
	
	private String 	CridDate 				= null;
	private String	Cupplier 				= null;
	private String 	Country					= null;
	private String 	City 					= null;
	private String	HotelName				= null;
	private String 	RateContract			= null;
	private String	InventoryContract 		= null;
	private String  RoomBedRatePlan			= null;
	private String 	PartnerType				= null;
	private String 	B2BPartnertype			= null;
	private String 	B2BPartner				= null;
	private String 	ApplicableRegion		= null;
	private String 	ApplicableCountry		= null;
	private String  BookingType				= null;
	private String 	BookingChannel			= null;
	private String 	StandardMargin			= null;
	private String 	AdditionalAdultMargin 	= null;
	private String  ChildMargin				= null;
	
	
	/**
	 * @return the cridDate
	 */
	public String getCridDate() {
		return CridDate;
	}
	/**
	 * @return the cupplier
	 */
	public String getCupplier() {
		return Cupplier;
	}
	/**
	 * @return the country
	 */
	public String getCountry() {
		return Country;
	}
	/**
	 * @return the city
	 */
	public String getCity() {
		return City;
	}
	/**
	 * @return the hotelName
	 */
	public String getHotelName() {
		return HotelName;
	}
	/**
	 * @return the rateContract
	 */
	public String getRateContract() {
		return RateContract;
	}
	/**
	 * @return the inventoryContract
	 */
	public String getInventoryContract() {
		return InventoryContract;
	}
	/**
	 * @return the roomBedRatePlan
	 */
	public String getRoomBedRatePlan() {
		return RoomBedRatePlan;
	}
	/**
	 * @return the partnerType
	 */
	public String getPartnerType() {
		return PartnerType;
	}
	/**
	 * @return the b2BPartnertype
	 */
	public String getB2BPartnertype() {
		return B2BPartnertype;
	}
	/**
	 * @return the b2BPartner
	 */
	public String getB2BPartner() {
		return B2BPartner;
	}
	/**
	 * @return the applicableRegion
	 */
	public String getApplicableRegion() {
		return ApplicableRegion;
	}
	/**
	 * @return the applicableCountry
	 */
	public String getApplicableCountry() {
		return ApplicableCountry;
	}
	/**
	 * @return the bookingType
	 */
	public String getBookingType() {
		return BookingType;
	}
	/**
	 * @return the bookingChannel
	 */
	public String getBookingChannel() {
		return BookingChannel;
	}
	/**
	 * @return the standardMargin
	 */
	public String getStandardMargin() {
		return StandardMargin;
	}
	/**
	 * @return the additionalAdultMargin
	 */
	public String getAdditionalAdultMargin() {
		return AdditionalAdultMargin;
	}
	/**
	 * @return the childMargin
	 */
	public String getChildMargin() {
		return ChildMargin;
	}
	/**
	 * @param cridDate the cridDate to set
	 */
	public void setCridDate(String cridDate) {
		CridDate = cridDate;
	}
	/**
	 * @param cupplier the cupplier to set
	 */
	public void setCupplier(String cupplier) {
		Cupplier = cupplier;
	}
	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		Country = country;
	}
	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		City = city;
	}
	/**
	 * @param hotelName the hotelName to set
	 */
	public void setHotelName(String hotelName) {
		HotelName = hotelName;
	}
	/**
	 * @param rateContract the rateContract to set
	 */
	public void setRateContract(String rateContract) {
		RateContract = rateContract;
	}
	/**
	 * @param inventoryContract the inventoryContract to set
	 */
	public void setInventoryContract(String inventoryContract) {
		InventoryContract = inventoryContract;
	}
	/**
	 * @param roomBedRatePlan the roomBedRatePlan to set
	 */
	public void setRoomBedRatePlan(String roomBedRatePlan) {
		RoomBedRatePlan = roomBedRatePlan;
	}
	/**
	 * @param partnerType the partnerType to set
	 */
	public void setPartnerType(String partnerType) {
		PartnerType = partnerType;
	}
	/**
	 * @param b2bPartnertype the b2BPartnertype to set
	 */
	public void setB2BPartnertype(String b2bPartnertype) {
		B2BPartnertype = b2bPartnertype;
	}
	/**
	 * @param b2bPartner the b2BPartner to set
	 */
	public void setB2BPartner(String b2bPartner) {
		B2BPartner = b2bPartner;
	}
	/**
	 * @param applicableRegion the applicableRegion to set
	 */
	public void setApplicableRegion(String applicableRegion) {
		ApplicableRegion = applicableRegion;
	}
	/**
	 * @param applicableCountry the applicableCountry to set
	 */
	public void setApplicableCountry(String applicableCountry) {
		ApplicableCountry = applicableCountry;
	}
	/**
	 * @param bookingType the bookingType to set
	 */
	public void setBookingType(String bookingType) {
		BookingType = bookingType;
	}
	/**
	 * @param bookingChannel the bookingChannel to set
	 */
	public void setBookingChannel(String bookingChannel) {
		BookingChannel = bookingChannel;
	}
	/**
	 * @param standardMargin the standardMargin to set
	 */
	public void setStandardMargin(String standardMargin) {
		StandardMargin = standardMargin;
	}
	/**
	 * @param additionalAdultMargin the additionalAdultMargin to set
	 */
	public void setAdditionalAdultMargin(String additionalAdultMargin) {
		AdditionalAdultMargin = additionalAdultMargin;
	}
	/**
	 * @param childMargin the childMargin to set
	 */
	public void setChildMargin(String childMargin) {
		ChildMargin = childMargin;
	} 	
	
	
	

}
