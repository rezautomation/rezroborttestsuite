package com.rezgateway.automation.Objects;

public class HotelBillingInfo {

	String hotelName, contractName, bankName, branch, accountNumber, swiftCode;

	public HotelBillingInfo getDetails(String[] values)
	{
		this.setHotelName		(values[0]);
		this.setContractName	(values[1]);
		this.setBankName		(values[2]);
		this.setBranch			(values[3]);
		this.setAccountNumber	(values[4]);
		this.setSwiftCode		(values[5]);
		return this;
	}
	
	public String getHotelName() {
		return hotelName;
	}

	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}

	public String getContractName() {
		return contractName;
	}

	public void setContractName(String contractName) {
		this.contractName = contractName;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getSwiftCode() {
		return swiftCode;
	}

	public void setSwiftCode(String swiftCode) {
		this.swiftCode = swiftCode;
	}
	
	
	
}
