package com.rezgateway.automation.Objects;

public class HotelContractInfo {

	String hotelName, hotelContractNumber, periodFrom, periodTo, allotment, allotmentType, freeSell, onRequest, selectAllotment, 
	selectRoom, contractname, currency, rateBy, apllicableRegion, applicalbeCountry, excludingCountry, partnerTypeDirect, 
	partnerTypeB2B, partnerTypeAffiliate, bookingChannelMobile, bookingChannelWeb, bookingChannelCC, bookingChannelWebService, 
	bookingTypeShoppingCart, bookingTypeDP, bookingTypeFP, daysOfTheWeek;

	public String getDaysOfTheWeek() {
		return daysOfTheWeek;
	}






	public void setDaysOfTheWeek(String daysOfTheWeek) {
		this.daysOfTheWeek = daysOfTheWeek;
	}






	public HotelContractInfo getDetails(String[] values)
	{
		this.setHotelName					(values[0]);
		this.setHotelContractNumber			(values[1]);
		this.setPeriodFrom					(values[2]);
		this.setPeriodTo					(values[3]);
		this.setAllotment					(values[4]);
		this.setAllotmentType				(values[5]);
		this.setFreeSell					(values[6]);
		this.setOnRequest					(values[7]);
		this.setSelectAllotment				(values[8]);
		this.setSelectRoom					(values[9]);
		this.setContractname				(values[10]);
		this.setCurrency					(values[11]);
		this.setRateBy						(values[12]);
		this.setApllicableRegion			(values[13]);
		this.setApplicalbeCountry			(values[14]);
		this.setExcludingCountry			(values[15]);
		this.setPartnerTypeDirect			(values[16]);
		this.setPartnerTypeB2B				(values[17]);
		this.setPartnerTypeAffiliate		(values[18]);
		this.setBookingChannelMobile		(values[19]);
		this.setBookingChannelWeb			(values[20]);
		this.setBookingChannelCC			(values[21]);
		this.setBookingChannelWebService	(values[22]);
		this.setBookingTypeShoppingCart		(values[23]);
		this.setBookingTypeDP				(values[24]);
		this.setBookingTypeFP				(values[25]);
		this.setDaysOfTheWeek				(values[26]);
		
		return this;
	}
	
	
	
	


	public String getAllotmentType() {
		return allotmentType;
	}






	public void setAllotmentType(String allotmentType) {
		this.allotmentType = allotmentType;
	}






	public String getHotelName() {
		return hotelName;
	}

	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}

	public String getHotelContractNumber() {
		return hotelContractNumber;
	}

	public void setHotelContractNumber(String hotelContractNumber) {
		this.hotelContractNumber = hotelContractNumber;
	}

	public String getPeriodFrom() {
		return periodFrom;
	}

	public void setPeriodFrom(String periodFrom) {
		this.periodFrom = periodFrom;
	}

	public String getPeriodTo() {
		return periodTo;
	}

	public void setPeriodTo(String periodTo) {
		this.periodTo = periodTo;
	}

	public String getAllotment() {
		return allotment;
	}

	public void setAllotment(String allotment) {
		this.allotment = allotment;
	}

	public String getFreeSell() {
		return freeSell;
	}

	public void setFreeSell(String freeSell) {
		this.freeSell = freeSell;
	}

	public String getOnRequest() {
		return onRequest;
	}

	public void setOnRequest(String onRequest) {
		this.onRequest = onRequest;
	}

	public String getSelectAllotment() {
		return selectAllotment;
	}

	public void setSelectAllotment(String selectAllotment) {
		this.selectAllotment = selectAllotment;
	}

	public String getSelectRoom() {
		return selectRoom;
	}

	public void setSelectRoom(String selectRoom) {
		this.selectRoom = selectRoom;
	}

	public String getContractname() {
		return contractname;
	}

	public void setContractname(String contractname) {
		this.contractname = contractname;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getRateBy() {
		return rateBy;
	}

	public void setRateBy(String rateBy) {
		this.rateBy = rateBy;
	}

	public String getApllicableRegion() {
		return apllicableRegion;
	}

	public void setApllicableRegion(String apllicableRegion) {
		this.apllicableRegion = apllicableRegion;
	}

	public String getApplicalbeCountry() {
		return applicalbeCountry;
	}

	public void setApplicalbeCountry(String applicalbeCountry) {
		this.applicalbeCountry = applicalbeCountry;
	}

	public String getExcludingCountry() {
		return excludingCountry;
	}

	public void setExcludingCountry(String excludingCountry) {
		this.excludingCountry = excludingCountry;
	}

	public String getPartnerTypeDirect() {
		return partnerTypeDirect;
	}

	public void setPartnerTypeDirect(String partnerTypeDirect) {
		this.partnerTypeDirect = partnerTypeDirect;
	}

	public String getPartnerTypeB2B() {
		return partnerTypeB2B;
	}

	public void setPartnerTypeB2B(String partnerTypeB2B) {
		this.partnerTypeB2B = partnerTypeB2B;
	}

	public String getPartnerTypeAffiliate() {
		return partnerTypeAffiliate;
	}

	public void setPartnerTypeAffiliate(String partnerTypeAffiliate) {
		this.partnerTypeAffiliate = partnerTypeAffiliate;
	}

	public String getBookingChannelMobile() {
		return bookingChannelMobile;
	}

	public void setBookingChannelMobile(String bookingChannelMobile) {
		this.bookingChannelMobile = bookingChannelMobile;
	}

	public String getBookingChannelWeb() {
		return bookingChannelWeb;
	}

	public void setBookingChannelWeb(String bookingChannelWeb) {
		this.bookingChannelWeb = bookingChannelWeb;
	}

	public String getBookingChannelCC() {
		return bookingChannelCC;
	}

	public void setBookingChannelCC(String bookingChannelCC) {
		this.bookingChannelCC = bookingChannelCC;
	}

	public String getBookingChannelWebService() {
		return bookingChannelWebService;
	}

	public void setBookingChannelWebService(String bookingChannelWebService) {
		this.bookingChannelWebService = bookingChannelWebService;
	}

	public String getBookingTypeShoppingCart() {
		return bookingTypeShoppingCart;
	}

	public void setBookingTypeShoppingCart(String bookingTypeShoppingCart) {
		this.bookingTypeShoppingCart = bookingTypeShoppingCart;
	}

	public String getBookingTypeDP() {
		return bookingTypeDP;
	}

	public void setBookingTypeDP(String bookingTypeDP) {
		this.bookingTypeDP = bookingTypeDP;
	}

	public String getBookingTypeFP() {
		return bookingTypeFP;
	}

	public void setBookingTypeFP(String bookingTypeFP) {
		this.bookingTypeFP = bookingTypeFP;
	}
	
	
}
