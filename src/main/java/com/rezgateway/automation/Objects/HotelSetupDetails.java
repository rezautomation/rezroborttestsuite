package com.rezgateway.automation.Objects;

import java.util.ArrayList;

public class HotelSetupDetails {

	ArrayList<HotelBillingInfo> 				billingInfo 			= new ArrayList<>();
	ArrayList<HotelCancellationPolicyDetails> 	cancellationPolicyInfo 	= new ArrayList<>();
	ArrayList<HotelContractInfo> 				contractinfo 			= new ArrayList<>();
	ArrayList<HotelRoomAndInventoryInfo> 		inventoryInfo 			= new ArrayList<>();
	ArrayList<HotelRoomCombinations> 			roomCombinations 		= new ArrayList<>();
	ArrayList<HotelStandardInfo> 				standardInfo 			= new ArrayList<>();
	ArrayList<HotelTaxDetails> 					taxInfo 				= new ArrayList<>();
	
	
	
	public ArrayList<HotelBillingInfo> getBillingInfo() {
		return billingInfo;
	}
	public void setBillingInfo(ArrayList<HotelBillingInfo> billingInfo) {
		this.billingInfo = billingInfo;
	}
	public ArrayList<HotelCancellationPolicyDetails> getCancellationPolicyInfo() {
		return cancellationPolicyInfo;
	}
	public void setCancellationPolicyInfo(
			ArrayList<HotelCancellationPolicyDetails> cancellationPolicyInfo) {
		this.cancellationPolicyInfo = cancellationPolicyInfo;
	}
	public ArrayList<HotelContractInfo> getContractinfo() {
		return contractinfo;
	}
	public void setContractinfo(ArrayList<HotelContractInfo> contractinfo) {
		this.contractinfo = contractinfo;
	}
	public ArrayList<HotelRoomAndInventoryInfo> getInventoryInfo() {
		return inventoryInfo;
	}
	public void setInventoryInfo(ArrayList<HotelRoomAndInventoryInfo> inventoryInfo) {
		this.inventoryInfo = inventoryInfo;
	}
	public ArrayList<HotelRoomCombinations> getRoomCombinations() {
		return roomCombinations;
	}
	public void setRoomCombinations(
			ArrayList<HotelRoomCombinations> roomCombinations) {
		this.roomCombinations = roomCombinations;
	}
	public ArrayList<HotelStandardInfo> getStandardInfo() {
		return standardInfo;
	}
	public void setStandardInfo(ArrayList<HotelStandardInfo> standardInfo) {
		this.standardInfo = standardInfo;
	}
	public ArrayList<HotelTaxDetails> getTaxInfo() {
		return taxInfo;
	}
	public void setTaxInfo(ArrayList<HotelTaxDetails> taxInfo) {
		this.taxInfo = taxInfo;
	}
}
