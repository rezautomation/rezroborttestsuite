package com.rezgateway.automation.Objects;

public class HotelTaxDetails {

	String hotelName, contractName, taxAndOtherCharges, from, to, salesTaxType, salesTaxValue, occupancyTaxType, 
	occupancyTaxValue, enrgyTaxType, enrgyTaxValue, maidServiceTaxType, maidServiceTax, resortFee, miscallaneousFee ;

	public HotelTaxDetails getDetails(String[] values)
	{
		this.setHotelName			(values[0]);
		this.setContractName		(values[1]);
		this.setTaxAndOtherCharges	(values[2]);
		this.setFrom				(values[3]);
		this.setTo					(values[4]);
		this.setSalesTaxType		(values[5]);
		this.setSalesTaxValue		(values[6]);
		this.setOccupancyTaxType	(values[7]);
		this.setOccupancyTaxValue	(values[8]);
		this.setEnrgyTaxType		(values[9]);
		this.setEnrgyTaxValue		(values[10]);
		this.setMaidServiceTaxType	(values[11]);
		this.setMaidServiceTax		(values[12]);
		this.setResortFee			(values[13]);
		this.setMiscallaneousFee	(values[14]);
		
		return this;
	}
	
	
	public String getMaidServiceTaxType() {
		return maidServiceTaxType;
	}


	public void setMaidServiceTaxType(String maidServiceTaxType) {
		this.maidServiceTaxType = maidServiceTaxType;
	}


	public String getHotelName() {
		return hotelName;
	}

	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}

	public String getContractName() {
		return contractName;
	}

	public void setContractName(String contractName) {
		this.contractName = contractName;
	}

	public String getTaxAndOtherCharges() {
		return taxAndOtherCharges;
	}

	public void setTaxAndOtherCharges(String taxAndOtherCharges) {
		this.taxAndOtherCharges = taxAndOtherCharges;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getSalesTaxType() {
		return salesTaxType;
	}

	public void setSalesTaxType(String salesTaxType) {
		this.salesTaxType = salesTaxType;
	}

	public String getSalesTaxValue() {
		return salesTaxValue;
	}

	public void setSalesTaxValue(String salesTaxValue) {
		this.salesTaxValue = salesTaxValue;
	}

	public String getOccupancyTaxType() {
		return occupancyTaxType;
	}

	public void setOccupancyTaxType(String occupancyTaxType) {
		this.occupancyTaxType = occupancyTaxType;
	}

	public String getOccupancyTaxValue() {
		return occupancyTaxValue;
	}

	public void setOccupancyTaxValue(String occupancyTaxValue) {
		this.occupancyTaxValue = occupancyTaxValue;
	}

	public String getEnrgyTaxType() {
		return enrgyTaxType;
	}

	public void setEnrgyTaxType(String enrgyTaxType) {
		this.enrgyTaxType = enrgyTaxType;
	}

	public String getEnrgyTaxValue() {
		return enrgyTaxValue;
	}

	public void setEnrgyTaxValue(String enrgyTaxValue) {
		this.enrgyTaxValue = enrgyTaxValue;
	}

	public String getMaidServiceTax() {
		return maidServiceTax;
	}

	public void setMaidServiceTax(String maidServiceTax) {
		this.maidServiceTax = maidServiceTax;
	}

	public String getResortFee() {
		return resortFee;
	}

	public void setResortFee(String resortFee) {
		this.resortFee = resortFee;
	}

	public String getMiscallaneousFee() {
		return miscallaneousFee;
	}

	public void setMiscallaneousFee(String miscallaneousFee) {
		this.miscallaneousFee = miscallaneousFee;
	}
	
	
}
