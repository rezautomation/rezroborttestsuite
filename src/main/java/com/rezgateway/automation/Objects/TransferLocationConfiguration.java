package com.rezgateway.automation.Objects;

public class TransferLocationConfiguration {

	String supplier = ""; 
	String country = ""; 
	String aToBoneway = ""; 
	String aToBround = ""; 
	String bToAoneway = "";
	String bToAround = ""; 
	String distance = ""; 
	String hours = ""; 
	String minuets = ""; 
	
	public TransferLocationConfiguration getDetails(String[] values)
	{
		this.setSupplier		(values[0]);
		this.setCountry			(values[1]);
		this.setaToBoneway		(values[2]);
		this.setaToBround		(values[3]);
		this.setbToAoneway		(values[4]);
		this.setbToAround		(values[5]);
		this.setDistance		(values[6]);
		this.setHours			(values[7]);
		this.setMinuets			(values[8]);
		
		return this;
	}
	
	public String getSupplier() {
		return supplier;
	}
	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getaToBoneway() {
		return aToBoneway;
	}
	public void setaToBoneway(String aToBoneway) {
		this.aToBoneway = aToBoneway;
	}
	public String getaToBround() {
		return aToBround;
	}
	public void setaToBround(String aToBround) {
		this.aToBround = aToBround;
	}
	public String getbToAoneway() {
		return bToAoneway;
	}
	public void setbToAoneway(String bToAoneway) {
		this.bToAoneway = bToAoneway;
	}
	public String getbToAround() {
		return bToAround;
	}
	public void setbToAround(String bToAround) {
		this.bToAround = bToAround;
	}
	public String getDistance() {
		return distance;
	}
	public void setDistance(String distance) {
		this.distance = distance;
	}
	public String getHours() {
		return hours;
	}
	public void setHours(String hours) {
		this.hours = hours;
	}
	public String getMinuets() {
		return minuets;
	}
	public void setMinuets(String minuets) {
		this.minuets = minuets;
	}
	
	
	
}
