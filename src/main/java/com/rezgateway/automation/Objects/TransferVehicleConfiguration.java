package com.rezgateway.automation.Objects;

public class TransferVehicleConfiguration {

	String supplier = ""; 
	String serviceType = ""; 
	String productType = ""; 
	String vehicleType = ""; 
	String maxPax = ""; 
	String maxBaggage = ""; 
	String maxHandLuggage = ""; 
	String seatAllocationAge = ""; 
	String chargeApplicableAge = ""; 
	String specialChildRate = "";
	
	public TransferVehicleConfiguration getDetails(String[] values)
	{
		this.setSupplier			(values[0]);
		this.setServiceType			(values[1]);
		this.setProductType			(values[2]);
		this.setVehicleType			(values[3]);
		this.setMaxPax				(values[4]);
		this.setMaxBaggage			(values[5]);
		this.setMaxHandLuggage		(values[6]);
		this.setSeatAllocationAge	(values[7]);
		this.setChargeApplicableAge	(values[8]);
		this.setSpecialChildRate	(values[9]);
		
		return this;
	}
	
	public String getSupplier() {
		return supplier;
	}
	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}
	public String getServiceType() {
		return serviceType;
	}
	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getVehicleType() {
		return vehicleType;
	}
	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}
	public String getMaxPax() {
		return maxPax;
	}
	public void setMaxPax(String maxPax) {
		this.maxPax = maxPax;
	}
	public String getMaxBaggage() {
		return maxBaggage;
	}
	public void setMaxBaggage(String maxBaggage) {
		this.maxBaggage = maxBaggage;
	}
	public String getMaxHandLuggage() {
		return maxHandLuggage;
	}
	public void setMaxHandLuggage(String maxHandLuggage) {
		this.maxHandLuggage = maxHandLuggage;
	}
	public String getSeatAllocationAge() {
		return seatAllocationAge;
	}
	public void setSeatAllocationAge(String seatAllocationAge) {
		this.seatAllocationAge = seatAllocationAge;
	}
	public String getChargeApplicableAge() {
		return chargeApplicableAge;
	}
	public void setChargeApplicableAge(String chargeApplicableAge) {
		this.chargeApplicableAge = chargeApplicableAge;
	}
	public String getSpecialChildRate() {
		return specialChildRate;
	}
	public void setSpecialChildRate(String specialChildRate) {
		this.specialChildRate = specialChildRate;
	}
	
	
}
