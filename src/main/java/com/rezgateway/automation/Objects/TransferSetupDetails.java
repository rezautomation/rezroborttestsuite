package com.rezgateway.automation.Objects;

import java.util.ArrayList;

public class TransferSetupDetails {

	ArrayList<TransferCityZoneConfiguration> 		zoneConfig 			= new ArrayList<>();
	ArrayList<TransferLocationConfiguration> 		locationConfig 		= new ArrayList<>();
	ArrayList<TransferStandardInfo> 				standardInfo 		= new ArrayList<>();
	ArrayList<TransferTimeConfiguration> 			timeConfig 			= new ArrayList<>();
	ArrayList<TransferVehicleConfiguration> 		vehicleConfig 		= new ArrayList<>();
	ArrayList<SupplierSetupDetails> 				supplier			= new ArrayList<>();
	ArrayList<TransferRouteAndVehicleInfo> 			routeList			= new ArrayList<>();
	ArrayList<TransferContractInfo> 				contractList		= new ArrayList<>();
	ArrayList<TransferCancellationPolicyDetails> 	cancellationList	= new ArrayList<>();
	ArrayList<TransferRateDetails> 					rateList			= new ArrayList<>();
	
	
	
	public ArrayList<TransferRateDetails> getRateList() {
		return rateList;
	}
	public void setRateList(ArrayList<TransferRateDetails> rateList) {
		this.rateList = rateList;
	}
	public ArrayList<TransferCityZoneConfiguration> getZoneConfig() {
		return zoneConfig;
	}
	public void setZoneConfig(ArrayList<TransferCityZoneConfiguration> zoneConfig) {
		this.zoneConfig = zoneConfig;
	}
	
	public ArrayList<TransferLocationConfiguration> getLocationConfig() {
		return locationConfig;
	}
	public void setLocationConfig(
			ArrayList<TransferLocationConfiguration> locationConfig) {
		this.locationConfig = locationConfig;
	}
	public ArrayList<TransferStandardInfo> getStandardInfo() {
		return standardInfo;
	}
	public void setStandardInfo(ArrayList<TransferStandardInfo> standardInfo) {
		this.standardInfo = standardInfo;
	}
	public ArrayList<TransferTimeConfiguration> getTimeConfig() {
		return timeConfig;
	}
	public void setTimeConfig(ArrayList<TransferTimeConfiguration> timeConfig) {
		this.timeConfig = timeConfig;
	}
	public ArrayList<TransferVehicleConfiguration> getVehicleConfig() {
		return vehicleConfig;
	}
	public void setVehicleConfig(
			ArrayList<TransferVehicleConfiguration> vehicleConfig) {
		this.vehicleConfig = vehicleConfig;
	}
	public ArrayList<SupplierSetupDetails> getSupplier() {
		return supplier;
	}
	public void setSupplier(ArrayList<SupplierSetupDetails> supplier) {
		this.supplier = supplier;
	}
	public ArrayList<TransferRouteAndVehicleInfo> getRouteList() {
		return routeList;
	}
	public void setRouteList(ArrayList<TransferRouteAndVehicleInfo> routeList) {
		this.routeList = routeList;
	}
	public ArrayList<TransferContractInfo> getContractList() {
		return contractList;
	}
	public void setContractList(ArrayList<TransferContractInfo> contractList) {
		this.contractList = contractList;
	}
	public ArrayList<TransferCancellationPolicyDetails> getCancellationList() {
		return cancellationList;
	}
	public void setCancellationList(
			ArrayList<TransferCancellationPolicyDetails> cancellationList) {
		this.cancellationList = cancellationList;
	}
	
	
	
	
	
}
