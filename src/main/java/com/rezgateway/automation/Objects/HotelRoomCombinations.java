package com.rezgateway.automation.Objects;

public class HotelRoomCombinations {

	String hotelname, contractName, roomType, bedType, ratePlan, inventory, cutoff, netrate, childrate, Select_allotment;

	public String getSelect_allotment() {
		return Select_allotment;
	}

	public void setSelect_allotment(String select_allotment) {
		Select_allotment = select_allotment;
	}

	public String getInventory() {
		return inventory;
	}

	public void setInventory(String inventory) {
		this.inventory = inventory;
	}

	public String getCutoff() {
		return cutoff;
	}

	public void setCutoff(String cutoff) {
		this.cutoff = cutoff;
	}

	public String getNetrate() {
		return netrate;
	}

	public void setNetrate(String netrate) {
		this.netrate = netrate;
	}

	public String getChildrate() {
		return childrate;
	}

	public void setChildrate(String childrate) {
		this.childrate = childrate;
	}

	public HotelRoomCombinations getDetails(String[] values) {
		this.setHotelname(values[0]);
		this.setContractName(values[1]);
		this.setRoomType(values[2]);
		this.setBedType(values[3]);
		this.setRatePlan(values[4]);
		this.setSelect_allotment(values[5]);
		this.setInventory(values[6]);
		this.setCutoff(values[7]);
		this.setNetrate(values[8]);
		this.setChildrate(values[9]);

		return this;
	}

	public String getHotelname() {
		return hotelname;
	}

	public void setHotelname(String hotelname) {
		this.hotelname = hotelname;
	}

	public String getContractName() {
		return contractName;
	}

	public void setContractName(String contractName) {
		this.contractName = contractName;
	}

	public String getRoomType() {
		return roomType;
	}

	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}

	public String getBedType() {
		return bedType;
	}

	public void setBedType(String bedType) {
		this.bedType = bedType;
	}

	public String getRatePlan() {
		return ratePlan;
	}

	public void setRatePlan(String ratePlan) {
		this.ratePlan = ratePlan;
	}

}
