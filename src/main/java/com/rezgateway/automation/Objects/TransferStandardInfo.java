package com.rezgateway.automation.Objects;

public class TransferStandardInfo {

	String action = ""; 
	String supplierName = ""; 
	String privateTransfers = ""; 
	String privateByZone = ""; 
	String privateByTime = ""; 
	String privateByDriverSpeakingLanguage = ""; 
	String sharedTransfers = ""; 
	String sharedByZone = ""; 
	String sharedByTime = "";
	String execute = "";
	
	public TransferStandardInfo getDetails(String[] values)
	{
		this.setAction							(values[0]);
		this.setSupplierName					(values[1]);
		this.setPrivateTransfers				(values[2]);
		this.setPrivateByZone					(values[3]);
		this.setPrivateByTime					(values[4]);
		this.setPrivateByDriverSpeakingLanguage	(values[5]);
		this.setSharedTransfers					(values[6]);
		this.setSharedByZone					(values[7]);
		this.setSharedByTime					(values[8]);
		this.setExecute							(values[9]);
		
		return this;
	}
	
	
	
	public String getExecute() {
		return execute;
	}
	public void setExecute(String execute) {
		this.execute = execute;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getSupplierName() {
		return supplierName;
	}
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	public String getPrivateTransfers() {
		return privateTransfers;
	}
	public void setPrivateTransfers(String privateTransfers) {
		this.privateTransfers = privateTransfers;
	}
	public String getPrivateByZone() {
		return privateByZone;
	}
	public void setPrivateByZone(String privateByZone) {
		this.privateByZone = privateByZone;
	}
	public String getPrivateByTime() {
		return privateByTime;
	}
	public void setPrivateByTime(String privateByTime) {
		this.privateByTime = privateByTime;
	}
	public String getPrivateByDriverSpeakingLanguage() {
		return privateByDriverSpeakingLanguage;
	}
	public void setPrivateByDriverSpeakingLanguage(
			String privateByDriverSpeakingLanguage) {
		this.privateByDriverSpeakingLanguage = privateByDriverSpeakingLanguage;
	}
	public String getSharedTransfers() {
		return sharedTransfers;
	}
	public void setSharedTransfers(String sharedTransfers) {
		this.sharedTransfers = sharedTransfers;
	}
	public String getSharedByZone() {
		return sharedByZone;
	}
	public void setSharedByZone(String sharedByZone) {
		this.sharedByZone = sharedByZone;
	}
	public String getSharedByTime() {
		return sharedByTime;
	}
	public void setSharedByTime(String sharedByTime) {
		this.sharedByTime = sharedByTime;
	}
	
	
}
