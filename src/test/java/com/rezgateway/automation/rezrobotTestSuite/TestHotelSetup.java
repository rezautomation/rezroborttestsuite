package com.rezgateway.automation.rezrobotTestSuite;

import static org.testng.Assert.assertTrue;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.rezgateway.automation.Objects.HotelCancellationPolicyDetails;
import com.rezgateway.automation.Objects.HotelContractInfo;
import com.rezgateway.automation.Objects.HotelSetupDetails;
import com.rezgateway.automation.Objects.HotelStandardInfo;
import com.rezgateway.automation.core.RunnerBase;
import com.rezgateway.automation.pages.CC.common.V11_CC_Com_HeaderPage;
import com.rezgateway.automation.pages.common.CC_Com_HomePage;
import com.rezgateway.automation.pages.common.CC_Com_LoginPage;
import com.rezgateway.automation.pages.hotelSetup.CC_HotelSetup_Contract;
import com.rezgateway.automation.pages.hotelSetup.CC_HotelSetup_Contract_CancellationPolicy_Page;
import com.rezgateway.automation.pages.hotelSetup.CC_HotelSetup_Contract_Popup_AssignRoomCombinations;
import com.rezgateway.automation.pages.hotelSetup.CC_HotelSetup_Contract_Popup_ContractDetails;
import com.rezgateway.automation.pages.hotelSetup.CC_HotelSetup_Contract_Popup_TaxDetails;
import com.rezgateway.automation.pages.hotelSetup.CC_HotelSetup_Standard;
import com.rezgateway.automation.processor.HotelSetupExcelHandler;
import com.rezgateway.automation.utill.*;

public class TestHotelSetup extends RunnerBase {

	private HashMap<String, String> portalInfo = new HashMap<>();
	private WebDriver driver = null;
	private CC_Com_HomePage home;
	private CC_HotelSetup_Standard standInfoPage;
	private CC_HotelSetup_Contract contractPage;
	private HotelSetupExcelHandler excelHandler = new HotelSetupExcelHandler();
	private HotelSetupDetails setupDetails;
	private HotelStandardInfo standardDetails;
	private CC_HotelSetup_Contract_Popup_ContractDetails contractInfoPopUp;
	private CC_HotelSetup_Contract_Popup_AssignRoomCombinations assignRoomPopUp;
	private ArrayList<String> contract = new ArrayList<String>();
	private CC_HotelSetup_Contract_Popup_TaxDetails taxDetailsPopUp;
	private CC_HotelSetup_Contract_CancellationPolicy_Page cnxPolicyPage;
	

	@BeforeClass
	public void testDriverInitial() throws Exception {

		portalInfo = readProperty(System.getProperty("user.dir") + File.separator + "Rezrobot_Details/Common/portalSpecificData/portalInfo.properties");
		initiateDriver();
		driver = DriverFactory.getInstance().getDriver();
		driver.manage().deleteAllCookies();

	}

	@Parameters({ "UserName", "Password" })
	@Test
	public void loginTest(String un, String pw) throws Exception {

		/******************************************************************/
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Login Test");
		result.setAttribute("Expected", "Should be logged in Successfully");
		/******************************************************************/

		driver.get(portalInfo.get("PortalUrlCC").concat(portalInfo.get("login")));
		CC_Com_LoginPage login = new CC_Com_LoginPage();
		home = login.loginWithValidCredentials(un, pw);
		assertTrue(home.isPageAvailable(), "Login Page Test");

	}

	@Test(dependsOnMethods = { "loginTest" })
	public void HotelSetupMenuLinkAvailability() throws Exception {

		/******************************************************************/
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Check the Hotel Setup Menu Link Availability ");
		result.setAttribute("Expected", " Should Display Hotel Setup Menu Link ");
		/******************************************************************/

		V11_CC_Com_HeaderPage contractMenu = home.navigateToContractMenu();

		assertTrue(contractMenu.isHotelSetupConfigurationLinkAvailbile(), "Hotel Setup Menu Link Availability Test ");

	}

	@Test(dependsOnMethods = { "HotelSetupMenuLinkAvailability" })
	public void testHotelSetupServiceAvailability() throws Exception {

		/******************************************************************/
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Check the Hotel Setup Service Availability ");
		result.setAttribute("Expected", " Should Display Hotel Setup Standard info page Service ");
		/******************************************************************/

		driver.get(portalInfo.get("PortalUrlCC").concat(portalInfo.get("hotelSetup")));
		standInfoPage = new CC_HotelSetup_Standard();
		assertTrue(standInfoPage.checkAvailability(), "Test Availability of Hotel Setup Standard info page");

	}

	@Test(dependsOnMethods = { "testHotelSetupServiceAvailability" })
	public void testHotelStandedDataSaveing() throws Exception {

		/******************************************************************/
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Check saveing the Details ");
		result.setAttribute("Expected", " Should Display Hotel Setup Standard info page Service ");
		/******************************************************************/

		setupDetails = excelHandler.getObject(portalInfo.get("hotelSetupExcel"));
		standardDetails = setupDetails.getStandardInfo().get(0);
		standInfoPage.enterDetails(standardDetails);
		assertTrue(standInfoPage.isSaveTheDetails(), "Test hotelStandard info Details Savaing Status");

	}

	@Test(dependsOnMethods = { "testHotelStandedDataSaveing" })
	public void testContractSectionAvailbility() throws Exception {

		/******************************************************************/
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Check Hotel Contract page Avilbility ");
		result.setAttribute("Expected", " Should Display Hotel Contract page ");
		/******************************************************************/

		contractPage = standInfoPage.proccedToContractPage();
		assertTrue(contractPage.checkAvailability(), "Test Hotel Contract page Avilbility ");

	}

	@Test(dependsOnMethods = { "testContractSectionAvailbility" })
	public void testAddingHotelRoom() throws Exception {
		/******************************************************************/
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Check Hotel Room adding and saving ");
		result.setAttribute("Expected", " Should add hotel Room ");
		/******************************************************************/
		contractPage = new CC_HotelSetup_Contract(setupDetails, standardDetails.getHotelName());
		assertTrue(contractPage.createRoomCombination(), "Test Adding Room combination");

	}

	@Test(dependsOnMethods = { "testAddingHotelRoom" })
	public void addContractDeatails() throws Exception {

		/******************************************************************/
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Check Hotel contract info Details saving ");
		result.setAttribute("Expected", " Should add Hotel contract info Details ");
		/******************************************************************/

		if (contractPage.proccedToConTractDetailsPopup() == null) {
			result.setAttribute("Actual", "Not Load the Contract Details popup screen");
			Assert.fail("not Load the Contract Details popup screen");
		} else {
			contractInfoPopUp = contractPage.proccedToConTractDetailsPopup();
			contract = contractInfoPopUp.enterDetails(setupDetails, standardDetails.getHotelName());
			ArrayList<HotelContractInfo> contractList = new ArrayList<HotelContractInfo>();
			for (int i = 0; i < setupDetails.getContractinfo().size(); i++) {
				if (setupDetails.getContractinfo().get(i).getHotelName().equals(standardDetails.getHotelName())) {
					contractList.add(setupDetails.getContractinfo().get(i));
				}
			}
			ArrayList<String> flag = new ArrayList<String>();
			int i = 0;
			for (String temp : contract) {
				if (temp.equals(contractList.get(i).getContractname())) {
					flag.add("true");
				} else {
					flag.add("false _" + temp + "_expected_" + contractList.get(i).getContractname());
				}
			}
			String test = flag.toString();
			if (test.contains("false")) {
				result.setAttribute("Actual", "Correctly Save the contracts");
			} else {
				result.setAttribute("Actual", "NOT  Save the contracts");
				Assert.fail("NOT  Save the contracts : " + test);
			}
		}
	}

	@Test(dependsOnMethods = { "addContractDeatails" })
	public void testAssignRoomCombination() throws Exception {

		/******************************************************************/
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", " Check assign the hotel room combination for contract ");
		result.setAttribute("Expected", " Should add Hotel assign room combination ");
		/******************************************************************/

		if (contractInfoPopUp.proccedToAssingRoomCombiination() == null) {
			result.setAttribute("Actual", "Not Load the assign Room combination screen");
			Assert.fail("Not Load the assign Room combination screen");
			
		} else {
			assignRoomPopUp = contractInfoPopUp.proccedToAssingRoomCombiination();
			if(assignRoomPopUp.enterDetails(setupDetails, standardDetails.getHotelName(), contract)){
				result.setAttribute("Actual", "Successfully save the assign Room combination screen details");
			}else{
				result.setAttribute("Actual", "Not save the assign Room combination screen details");
				Assert.fail("Not save the assign Room combination details");
			}
		}
	}
	
	
	@Test(dependsOnMethods = { "testAssignRoomCombination" })
	public void testTaxDetailsScreen() throws Exception {
		
		/******************************************************************/
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", " Check adding Tax Details for contract ");
		result.setAttribute("Expected", " Should add Tax Details for contract");
		/******************************************************************/
		
		if (assignRoomPopUp.proceedToTaxDetailsScreen() == null) {
			result.setAttribute("Actual", "Not Load the Tax screen");
			Assert.fail("Not Load the Tax screen");
			
		} else {
			taxDetailsPopUp = new CC_HotelSetup_Contract_Popup_TaxDetails();
			if(taxDetailsPopUp.enterDetailsInScreen(setupDetails, standardDetails.getHotelName())){
				result.setAttribute("Actual", "Successfully save the Tax details");
				//DriverFactory.getInstance().getDriver().findElement(By.)
			}else{
				result.setAttribute("Actual", "Not save the Tax details");
				Assert.fail("Not save the Tax details");
			}
		}
		contractPage = taxDetailsPopUp.closeTheHotelContractConfigScreen();
	}

	
	@Test(dependsOnMethods = { "testTaxDetailsScreen" })
	public void testSavedContractAvilability() throws Exception {
		
		/******************************************************************/
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", " Verify the Hotel Contract is added to the Contract information grid ");
		result.setAttribute("Expected", " Should display hotel contract is added to the 'Contract information' grid");
		/******************************************************************/
		
		if(contractPage.isContractSave()){
			result.setAttribute("Actual", "Successfully save and added to contract info grid");
		}else{
			result.setAttribute("Actual", "Not added to contract info grid");
			Assert.fail("Not added to contract info grid");
		}
	}
	
	@Test(dependsOnMethods = { "testSavedContractAvilability" })
	public void testAddingCancellationPolicy() throws Exception {
		
		/******************************************************************/
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", " Test the Adding Cancellation policy for Contracts ");
		result.setAttribute("Expected", " Should be saved Cancellation policy for Contracts");
		/******************************************************************/
		
		cnxPolicyPage = contractPage.proccedToCancellationPolicyPage();
		ArrayList<HotelCancellationPolicyDetails> cancellationPolicyInfo  = setupDetails.getCancellationPolicyInfo();
		ArrayList<HotelContractInfo> inventoryContract = setupDetails.getContractinfo();
		if(cnxPolicyPage.enterDetails(cancellationPolicyInfo ,inventoryContract )){
			result.setAttribute("Actual", "Successfully adding the Cancellation policy for all contract");
		}else{
			result.setAttribute("Actual", "Successfully save and added to contract info grid");
			Assert.fail("Not added to contract info grid");
		}
	}

	@AfterClass
	public void tearDown() {
		DriverFactory.getInstance().getDriver().quit();

	}

}
