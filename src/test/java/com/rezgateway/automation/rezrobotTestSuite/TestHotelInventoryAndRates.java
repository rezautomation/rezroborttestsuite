package com.rezgateway.automation.rezrobotTestSuite;

import static org.testng.Assert.assertTrue;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.rezgateway.automation.Objects.HotelContractInfo;
import com.rezgateway.automation.Objects.HotelRoomCombinations;
import com.rezgateway.automation.Objects.HotelSetupDetails;
import com.rezgateway.automation.Objects.HotelStandardInfo;
import com.rezgateway.automation.core.RunnerBase;
import com.rezgateway.automation.dataObject.V11_Hotel_Contracts_Object;
import com.rezgateway.automation.pages.CC.common.V11_CC_Com_HeaderPage;
import com.rezgateway.automation.pages.CC.common.V11_CC_Com_HotelName_Lookup;
import com.rezgateway.automation.pages.CC.common.V11_CC_Com_Lookup;
import com.rezgateway.automation.pages.backoffice.V11_CC_Hotel_Inventory_Rates_Contract_Selection_Page;
import com.rezgateway.automation.pages.backoffice.V11_CC_Hotel_Inventory_Rates_Dates_Period_Selection_Page;
import com.rezgateway.automation.pages.backoffice.V11_CC_Select_Inventory_Allotment_Page;
import com.rezgateway.automation.pages.backoffice.V11_CC_Select_Room_Types_Page;
import com.rezgateway.automation.pages.backoffice.V11_UpdateRateOptions_Page;
import com.rezgateway.automation.pages.common.CC_Com_HomePage;
import com.rezgateway.automation.pages.common.CC_Com_LoginPage;
import com.rezgateway.automation.processor.HotelSetupExcelHandler;
import com.rezgateway.automation.utill.DriverFactory;

public class TestHotelInventoryAndRates extends RunnerBase {

	private HashMap<String, String> portalInfo = new HashMap<>();
	private WebDriver driver = null;

	private CC_Com_HomePage home;
	private V11_CC_Com_HeaderPage contractMenu;
	private V11_CC_Hotel_Inventory_Rates_Dates_Period_Selection_Page datesperiodPage;
	private V11_Hotel_Contracts_Object contractObj;
	private V11_CC_Select_Room_Types_Page roomtypesPage;
	private V11_CC_Hotel_Inventory_Rates_Contract_Selection_Page contractPage;
	private V11_CC_Com_HotelName_Lookup hotelnamelookPage;
	private V11_CC_Select_Inventory_Allotment_Page inventoryTypePage;
	private V11_CC_Com_Lookup comlookupPage;
	private V11_UpdateRateOptions_Page rateoptionsPage;

	private HotelSetupDetails setupDetails;
	private HotelSetupExcelHandler excelHandler = new HotelSetupExcelHandler();
	private HotelStandardInfo standardDetails;

	private ArrayList<HotelContractInfo> contractList;
	private ArrayList<HotelRoomCombinations> roominfolist;

	@BeforeClass
	public void testDriverInitial() throws Exception {

		portalInfo = readProperty(System.getProperty("user.dir") + File.separator + "Rezrobot_Details/Common/portalSpecificData/portalInfo.properties");
		initiateDriver();
		driver = DriverFactory.getInstance().getDriver();
		driver.manage().deleteAllCookies();

	}

	@Parameters({ "UserName", "Password" })
	@Test
	public void loginTest(String un, String pw) throws Exception {

		/******************************************************************/
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Login Test");
		result.setAttribute("Expected", "Should be logged in Successfully");
		/******************************************************************/

		driver.get(portalInfo.get("PortalUrlCC").concat(portalInfo.get("login")));
		CC_Com_LoginPage login = new CC_Com_LoginPage();
		home = login.loginWithValidCredentials(un, pw);
		assertTrue(home.isPageAvailable(), "Login Page Test");

	}

	@Test(dependsOnMethods = { "loginTest" })
	public void inventoryAndRateSetupMenuLinkAvailability() throws Exception {

		/******************************************************************/
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Check the Inventory and Rates Setup Menu Link Availability ");
		result.setAttribute("Expected", " Should Display  Inventory and Rates Setup Menu Link ");
		/******************************************************************/

		contractMenu = home.navigateToContractMenu();
		assertTrue(contractMenu.isHotelInventoryAndRateLinkAvailbile(), "Inventory and Rates Setup Menu Link Availability Test ");

	}

	@Test(dependsOnMethods = { "inventoryAndRateSetupMenuLinkAvailability" })
	public void testInventorySetupServiceAvailability() throws Exception {

		/******************************************************************/
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Check Inventory and Rates Setup Service Availability ");
		result.setAttribute("Expected", " Should Display Inventory and Rates Setup Screen ");
		/******************************************************************/

		driver.get(portalInfo.get("PortalUrlCC").concat(portalInfo.get("inventoryAndRateSetup")));
		driver.switchTo().frame("inventoryRateIframe");
		boolean flag = driver.findElement(By.xpath("//*[@id='root']/div/div[1]/div/form/div[2]/div/input")).isDisplayed();
		assertTrue(flag, "Check Inventory and Rates Setup Service Availability");

	}

	@Test(dependsOnMethods = { "testInventorySetupServiceAvailability" })
	public void testHotelNameInTheHotelLookup() throws Exception {

		/******************************************************************/
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Check Hotel Name In The HotelLookup screen ");
		result.setAttribute("Expected", " Should Display Inventory and Rates Setup Screen ");
		/******************************************************************/
		setupDetails = excelHandler.getObject(portalInfo.get("hotelSetupExcel"));
		standardDetails = setupDetails.getStandardInfo().get(0);

		DriverFactory.getInstance().getDriver().findElement(By.xpath("//*[@id='root']/div/div[1]/div/form/div[2]/div/input")).sendKeys(standardDetails.getHotelName());
		comlookupPage = new V11_CC_Com_Lookup();
		comlookupPage.loadLookUp();
		hotelnamelookPage = new V11_CC_Com_HotelName_Lookup();

		if (hotelnamelookPage.isHotelAvailable().size() >= 1) {
			result.setAttribute("Actual", "Hotel name is Availabilie");
			hotelnamelookPage.selectFirstRecords();
		} else {
			contractPage = new V11_CC_Hotel_Inventory_Rates_Contract_Selection_Page();
			contractPage = hotelnamelookPage.procedToInventoryRateContractSection();
			result.setAttribute("Actual", "Hotel name is not Availabilie");
			Assert.fail("Hotel name is not availbile");
		}

	}

	@Test(dependsOnMethods = { "testHotelNameInTheHotelLookup" })
	public void testInventoryContractAvaililty() throws Exception {

		/******************************************************************/
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Check Inventory Contract Availability  in the Screen ");
		result.setAttribute("Expected", " Should Display all the Inventory contracts ");
		/******************************************************************/

		contractPage = new V11_CC_Hotel_Inventory_Rates_Contract_Selection_Page();
		int inCount = getContractCount();
		if (inCount == contractPage.getInventoryContractList().size()) {
			result.setAttribute("Actual", "All the Contracts are available");
		} else {
			result.setAttribute("Actual", "Failed  actual : " + contractPage.getInventoryContractList().size() + "Expected : " + inCount);
			Assert.fail("Failed  actual : " + contractPage.getInventoryContractList().size() + "Expected : " + inCount);
		}
	}

	@Test(dependsOnMethods = { "testInventoryContractAvaililty" })
	public void testRateContractAvaililty() throws Exception {

		/******************************************************************/
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Check rate Contract Availability  in the Screen ");
		result.setAttribute("Expected", " Should Display all the rate contracts ");
		/******************************************************************/

		Iterator<Entry<WebElement, ArrayList<WebElement>>> itr = contractPage.getRateContractMap().entrySet().iterator();
		Iterator<Entry<String, String[]>> rateContracMap = getRateContractCount().entrySet().iterator();
		ArrayList<String> flag = new ArrayList<String>();

		while (itr.hasNext()) {
			if (rateContracMap.next().getValue().length == itr.next().getValue().size()) {
				flag.add(itr.next().getKey().getText() + "_" + itr.next().getValue().size() + "_true");
			} else {
				flag.add(itr.next().getKey().getText() + "_" + itr.next().getValue().size() + "_false");
			}
		}

		String flags = flag.toString();

		if (flags.contains("false")) {
			result.setAttribute("Actual", "some rate contracts are missing");
			Assert.fail("some rate contracts are missing");
		} else {
			result.setAttribute("Actual", "All the rate contract are availbile");
		}
	}

	@Test(dependsOnMethods = { "testRateContractAvaililty" })
	public void testAddingRate() throws Exception {

		/******************************************************************/
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Check rates Adding for room ");
		result.setAttribute("Expected", " Should add rates for room ");
		/******************************************************************/

		getContractList();

		// select 1st Rate contract of the 1st Inventory Contract
		contractPage.selectSpecificContract(contractList.get(0));
		datesperiodPage = new V11_CC_Hotel_Inventory_Rates_Dates_Period_Selection_Page();

		// Select all days option
		DriverFactory.getInstance().getDriver().findElement(By.xpath("//*[@id='root']/div/div[1]/div/div/div[1]/div[2]/div/div[2]/div[3]/div[2]/label/div/label[1]/input")).click();

		// Select from date
		datesperiodPage.LoadCalenderFromDate();
		String datefrom = contractList.get(0).getPeriodFrom();
		datesperiodPage.SelectDateperiod(datefrom);

		// Select To Date
		datesperiodPage.LoadCalenderToDate();
		String dateTo = contractList.get(0).getPeriodTo();
		datesperiodPage.SelectDateperiod(dateTo);

		// Select the Days of week
		datesperiodPage.selectDaysOftheWeek(contractList.get(0));
		datesperiodPage.clickAddButton();
		inventoryTypePage = datesperiodPage.proceedInventoryAllotmentPage();

		DriverFactory.getInstance().getDriver().wait(2);
		
		boolean flag = false;
		//want check  available INventory and allotment 
		if (flag) {
			// select one room and add rates
			getRoomCombination();
			inventoryTypePage.selectInventoryAllotmentTypes(roominfolist.get(0), contractList.get(0));
			
			//want to check Available room type against to Expected Results
			roomtypesPage = new V11_CC_Select_Room_Types_Page();
			boolean flag2 = false;
			if(flag2){
				
				//Select Room Types
				roomtypesPage.Select_Room_Types(roominfolist.get(0), contractList.get(0));
				
				//Select View / Update Inventory Options
				roomtypesPage.SelectInventory_Options(contractObj);
				rateoptionsPage =  new V11_UpdateRateOptions_Page();
						
				//Select View / Update Rate Options
				rateoptionsPage.selectRateoptions(contractObj);
				
				//Select Periods to Modify for Enter the Rates
				rateoptionsPage.selectRatePeriod(contractList.get(0));
				
				//Enter the Rates and Save the Rate for One Room 
				rateoptionsPage.enterInventoriesandRates(roominfolist.get(0), contractList.get(0));
				
			}else{
				result.setAttribute("Actual", "Room types are missing ");
				Assert.fail("Some Room types are missing ");
			}

		} else {
			result.setAttribute("Actual", "Some Inventory and Allotment types are missing ");
			Assert.fail("Some Inventory and Allotment types are missing");
		}

	}

	
	
	
	
	
	public ArrayList<HotelContractInfo> getContractList() {

		contractList = new ArrayList<HotelContractInfo>();

		for (int j = 0; j < setupDetails.getContractinfo().size(); j++) {
			if (setupDetails.getContractinfo().get(j).getHotelName().equals(standardDetails.getHotelName())) {
				contractList.add(setupDetails.getContractinfo().get(j));
			}
		}

		return contractList;
	}

	public ArrayList<HotelRoomCombinations> getRoomCombination() {

		roominfolist = new ArrayList<>();

		for (int k = 0; k < setupDetails.getRoomCombinations().size(); k++) {
			if (setupDetails.getRoomCombinations().get(k).getHotelname().equals(standardDetails.getHotelName())) {
				roominfolist.add(setupDetails.getRoomCombinations().get(k));
			}
		}

		return roominfolist;
	}

	public int getContractCount() {
		int count = 1;
		ArrayList<HotelContractInfo> invContract = setupDetails.getContractinfo();
		for (HotelContractInfo tempObj : invContract) {

			if (!Integer.toString(count).equals(tempObj.getHotelContractNumber())) {
				count++;
			}
		}

		return count;
	}

	@SuppressWarnings("null")
	public Map<String, String[]> getRateContractCount() {

		Map<String, String[]> ratecontractMap = new HashMap<String, String[]>();
		ArrayList<HotelContractInfo> invContract = setupDetails.getContractinfo();
		int contractcount = 1;
		int rateCount = 0;
		String rateContractCount[] = null;
		// This method works only for sort by contract number

		int i = 0;
		for (HotelContractInfo tempObj : invContract) {

			if (Integer.toString(contractcount).equals(tempObj.getHotelContractNumber())) {
				rateCount++;
				rateContractCount[i] = Integer.toString(rateCount);
			} else {
				ratecontractMap.put(Integer.toString(contractcount), rateContractCount);
				contractcount++;
			}
			i++;
		}
		return ratecontractMap;
	}

	@AfterClass
	public void tearDown() {
		DriverFactory.getInstance().getDriver().quit();

	}

}
